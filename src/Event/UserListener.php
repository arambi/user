<?php 

namespace User\Event;

use Cake\Event\EventListenerInterface;
use Cake\ORM\TableRegistry;

class UserListener implements EventListenerInterface
{
  public function implementedEvents()
  {
    return [
      'Manager.View.Sites.beforeBuild' => 'websiteBuild',
    ];
  }

  public function websiteBuild( $event, $table)
  {
    $view = $event->subject();

    $table->crud->addFields([
      'settings.users_logued_after_login' => [
        'label' => __d( 'admin', 'Login automático después del registro'),
        'help' => __d( 'admin', 'Después de que los usuarios se hayan registrado, hacer login automáticamente'),
        'type' => 'boolean'
      ],
      'settings.users_reply_email' => [
        'label' => __d( 'admin', 'Email de respuesta a usuarios'),
        'help' => __d( 'admin', 'Dirección de correo a donde podrán responder los usuarios en los emails que se les envía'),
        'type' => 'string'
      ],
      'settings.new_users_notice_email' => [
        'label' => __d( 'admin', 'Email de aviso de nuevos usuarios'),
        'help' => __d( 'admin', 'Dirección de correo a donde llegarán avisos de nuevos usuarios'),
        'type' => 'string'
      ],
      'settings.comments_email' => [
        'label' => __d( 'admin', 'Email donde se recibirán los comentarios'),
        'help' => __d( 'admin', 'Email donde se recibirán los comentarios que hacen los usuarios en el web'),
        'type' => 'string'
      ],
    ]);

    $view->addColumn( 'update', [
      'title' => __d( 'admin', 'Usuarios'),
      'box' => [
        [
          'elements' => [
            'settings.users_logued_after_login',
            'settings.users_reply_email',
            'settings.new_users_notice_email',
            'settings.comments_email'
          ]
        ]
      ]
    ]);
  }

} 