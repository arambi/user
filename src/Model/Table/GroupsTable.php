<?php

namespace User\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Manager\Navigation\NavigationCollection;
use Cake\Database\Type;
use Cake\Database\Schema\Table as Schema;
use User\Database\Type\MenuType;
use Cake\Core\Configure;

//use Acl\Model\Behavior\AclBehavior;
/**
 * Groups Model
 */
class GroupsTable extends Table
{

	/**
	 * Initialize method
	 *
	 * @param array $config The configuration for the Table.
	 * @return void
	 */
	public function initialize(array $config)
	{
		Type::map('user.menu', MenuType::class);
		$this->table('groups');
		$this->displayField('name');
		$this->primaryKey('id');


		// Behaviors
		$this->addBehavior('Timestamp');
		$this->addBehavior('Cofree.Jsonable', [
			'fields' => ['permissions']
		]);

		$this->addBehavior('Manager.Crudable');
		$this->addBehavior('Cofree.Saltable');

		$this->addBehavior('I18n.I18nTranslate', [
			'fields' => [
				'login_flash_message',
				'register_flash_message',
				'logout_flash_message'
			],
		]);


		// CRUD
		$this->crud
			->addJsFiles(['/user/js/user_controller.js'])
			->addFields([
				'export' => [
					'label' => __d('admin', 'Descargar CSV'),
					'type' => 'export',
					'link' => function ($field) {
						// _d( $field->table()->crud->getContent());
						// return 'Hola';
					}
				],
				'name' => __d('admin', 'Nombre'),
				'level' => [
					'label' => 'Nivel',
					'show' => 'administrator.superadmin'
				],
				'by_default' => [
					'label' => __d('admin', 'Por defecto al registrarse'),
					'help' => __d('admin', 'Grupo por defecto al que se vincularán los usuarios registrados')
				],
				'slug' => [
					'label' => __d('admin', 'Nombre computerizado'),
					'help' => __d('admin', 'Nombre usado por el programador'),
					'show' => 'administrator.superadmin'
				],
				'redirect_login_section_id' => [
					'label' => __d('admin', 'Redirección después de login'),
					'type' => 'select',
					'options' => function ($crud) {
						return TableRegistry::get('Section.Sections')->selectOptions();
					},
					'help' => __d('admin', 'Página a donde se redirigirá al usuario después de loguearse')
				],

				'redirect_register_section_id' => [
					'label' => __d('admin', 'Redirección después de registro'),
					'type' => 'select',
					'options' => function ($crud) {
						return TableRegistry::get('Section.Sections')->selectOptions();
					},
					'help' => __d('admin', 'Página a donde se redirigirá al usuario después de registarse')
				],

				'login_flash' => [
					'label' => __d('admin', 'Mostrar mensaje flotante en login'),
					'help' => __d('admin', 'Cuando los usuarios hagan login se mostrará un mensaje flotante'),
					'type' => 'boolean'
				],

				'login_flash_message' => [
					'label' => __d('admin', 'Mensaje flotante de login'),
					'type' => 'text',
					'help' => __d('admin', 'Mensaje flotante a mostrar cuando los usuarios hagan login'),
					'show' => 'content.login_flash',
				],

				'logout_flash' => [
					'label' => __d('admin', 'Mostrar mensaje flotante en logout'),
					'help' => __d('admin', 'Cuando los usuarios hagan logout se mostrará un mensaje flotante'),
					'type' => 'boolean'
				],

				'logout_flash_message' => [
					'label' => __d('admin', 'Mensaje flotante de logout'),
					'type' => 'text',
					'help' => __d('admin', 'Mensaje flotante a mostrar cuando los usuarios hagan logout'),
					'show' => 'content.login_flash',
				],

				'register_flash' => [
					'label' => __d('admin', 'Mostrar mensaje flotante al registrar'),
					'help' => __d('admin', 'Cuando los usuarios se registren se mostrará un mensaje flotante'),
					'type' => 'boolean'
				],

				'register_flash_message' => [
					'label' => __d('admin', 'Mensaje flotante de registro'),
					'type' => 'text',
					'help' => __d('admin', 'Mensaje flotante a mostrar cuando los usuarios se registren'),
					'show' => 'content.register_flash'
				],

				'redirect_logout_section_id' => [
					'label' => __d('admin', 'Redirección después de logout'),
					'type' => 'select',
					'options' => function ($crud) {
						return TableRegistry::get('Section.Sections')->selectOptions();
					},
					'help' => __d('admin', 'Página a donde se redirigirá al usuario después de salir de la aplicación')
				],
				'permissions' => [
					'label' => __d('admin', 'Permisos'),
					'type' => 'checkbox',
					'template' => 'User.fields.permissions',

				],
				'menu' => [
					'label' => __d('admin', 'Menu'),
					'type' => 'string',
					'options' => function ($crud) {
						$nodes = $this->adminMenuElements($crud->getContent());
						return $nodes;
					},
					'template' => 'User.fields.menu'
				]
			])
			->setName([
				'singular' => __d('admin', 'Grupos de usuarios'),
				'plural' => __d('admin', 'Grupo de usuarios'),
			])
			->addIndex('index', [
				'fields' => [
					'name',
				],
				'actionButtons' => ['create']
			])
			->addIndex('menus', [
				'fields' => [
					'name',
				],
				'actionLink' => 'menu',
				'actionButtons' => [],
				'template' => 'Manager/index'
			])
			->addView('update', [
				'columns' => [
					[
						'key' => 'general',
						'box' => [
							[
								'title' => 'Edición',
								'elements' => [
									'export',
									'name',
									'by_default',
									'slug',
									'level',
								]
							],
							[
								'title' => 'Redirecciones',
								'elements' => [
									'redirect_login_section_id',
									'redirect_register_section_id',
									'redirect_logout_section_id',
								]
							],
							[
								'title' => 'Mensajes flotantes',
								'elements' => [
									'login_flash',
									'login_flash_message',
									'register_flash',
									'register_flash_message',
									'logout_flash',
									'logout_flash_message',
								]
							],
							[
								'title' => 'Permisos',
								'elements' => [
									'permissions',
								]
							]
						]
					]
				],
				'actionButtons' => ['index']
			], ['create'])

			->addView('menu', [
				'title' => __d('admin', 'Menú de administración'),
				'columns' => [
					[
						'box' => [
							[
								'title' => 'Edición',
								'elements' => [
									'name',
									'menu'
								]
							]
						]
					]
				],
				'template' => 'Manager/update',
				'actionButtons' => ['index']
			]);
	}

	/**
	 * Default validation rules.
	 *
	 * @param \Cake\Validation\Validator $validator
	 * @return \Cake\Validation\Validator
	 */
	public function validationDefault(Validator $validator)
	{
		$validator
			->add('id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('id', 'create')
			->allowEmpty('name')
			->allowEmpty('slug')
			->add('level', 'valid', ['rule' => 'numeric'])
			->allowEmpty('level')
			->allowEmpty('permissions');

		return $validator;
	}

	public function adminMenuElements($content)
	{
		if (empty($content['id'])) {
			return;
		}
		$items = NavigationCollection::getAll($content['id']);
		$return = [];

		foreach ($items as &$item) {
			if ($item['url']) {
				$return[] = [
					'key' => $item['key'],
					'icon' => $item['icon'],
					'label' => $item['label'],
					'nodes' => []
				];
			}
		}

		return $return;
	}

	protected function _initializeSchema(Schema $schema)
	{
		$schema->columnType('menu', 'user.menu');
		return $schema;
	}
}
