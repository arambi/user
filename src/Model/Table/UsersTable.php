<?php

namespace User\Model\Table;

use ArrayObject;
use I18n\Lib\Lang;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\Entity;
use Cake\Event\Event;
use Cake\Utility\Text;
use Cake\Core\Configure;
use Cake\Routing\Router;
use Website\Lib\Website;
use Cake\Network\Request;
use Cake\Network\Session;
use Cake\Network\Response;
use Cake\Utility\Security;
use Cofree\Lib\EmailSender;
use Cake\Network\Email\Email;
use Cake\Validation\Validator;
use Cake\Auth\FormAuthenticate;
use Cake\Controller\Controller;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Datasource\EntityInterface;
use Cake\Controller\ComponentRegistry;
use Cake\Database\Expression\QueryExpression;
use Cake\Event\EventManager;

/**
 * Users Model
 */
class UsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->setTable('users');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        // Behaviors
        $this->addBehavior('Timestamp');
        $this->addBehavior('Manager.Crudable');
        $this->addBehavior('Cofree.Saltable');

        // Associations
        $this->belongsTo('Groups', [
            'foreignKey' => 'group_id',
            'className' => 'User.Groups',
        ]);

        $this->crud->associations(['Groups']);
        $this->crud
            ->addFields([
                'group' => [
                    'label' => __d('admin', 'Grupo'),
                    'options' => function ($crud) {
                        if (!empty($_SESSION['Auth']['User']['group']['level'])) {
                            $options[] = ['id' => null, $this->getDisplayField() => __d('admin', '-- Selecciona --')];
                            $options = array_merge($options, $this->Groups->find()
                                ->where([
                                    'Groups.level >=' => $_SESSION['Auth']['User']['group']['level']
                                ])
                                ->toArray());

                            return $options;
                        }

                        return [];
                    }
                ],
                'name' => __d('admin', 'Nombre'),
                'email' => __d('admin', 'Correo electrónico'),
                'current_password' => [
                    'label' => __d('admin', 'Escribe la actual contraseña'),
                    'type' => 'password',
                    'show' => 'content.change_password'
                ],
                'password' => [
                    'label' => __d('admin', 'Contraseña'),
                    'type' => 'password',
                    'show' => 'content.change_password'
                ],
                'password2' => [
                    'label' => __d('admin', 'Repite la contraseña'),
                    'type' => 'password',
                    'show' => 'content.change_password'
                ],
                'status'  => [
                    'label' => __d('admin', 'Estado'),
                    'type' => 'select',
                    'options' => [
                        'inactive' => __d('admin', 'Inactivo'),
                        'pending' => __d('admin', 'Pendiente de confirmación'),
                        'active' => __d('admin', 'Activo'),
                    ],
                    'empty' => __d('admin', '-- Selecciona --'),
                ],
                'confirm_url' => [
                    'label' => __d('admin', 'URL de confirmación'),
                    'type' => 'info',
                    'show' => 'content.status == "pending"'
                ],
                'change_password' => [
                    'label' => __d('admin', 'Cambiar contraseña'),
                    'type' => 'boolean'
                ]
            ])
            ->setName([
                'singular' => __d('admin', 'Usuario'),
                'plural' => __d('admin', 'Usuarios'),
            ])
            ->addIndex('index', [
                'fields' => [
                    'name',
                    'email',
                    'group',
                    'status',
                ],
                'actionButtons' => ['create'],
            ])
            ->addView('update', [
                'columns' => [
                    [
                        'key' => 'general',
                        'cols' => 8,
                        'box' => [
                            [
                                'key' => 'general',
                                'elements' => [
                                    'group',
                                    'name',
                                    'email',
                                    'status',
                                    'confirm_url',
                                ]
                            ]
                        ]
                    ]
                ],
                'actionButtons' => ['index', 'create'],
            ])
            ->addView('data', [
                'columns' => [
                    [
                        'key' => 'general',
                        'cols' => 8,
                        'box' => [
                            [
                                'elements' => [
                                    'name',
                                    'email',
                                    'change_password',
                                    'current_password',
                                    'password',
                                    'password2',
                                ]
                            ]
                        ]
                    ]
                ],
                'template' => 'Manager/update',
                'actionButtons' => [],
            ])
            ->addView('create', [
                'columns' => [
                    [
                        'key' => 'general',
                        'cols' => 8,
                        'box' => [
                            [
                                'key' => 'general',
                                'elements' => [
                                    'group',
                                    'name',
                                    'email',
                                ]
                            ]
                        ]
                    ]
                ],
                'actionButtons' => ['index']
            ]);
    }

    public function beforeSave(Event $event, EntityInterface $entity, ArrayObject $options)
    {
        if ($entity->isNew() && empty($entity->locale)) {
            $entity->set('locale', Lang::current('iso3'));
        }

        if ($entity->isNew()) {
            $entity->set('confirm_email_request_date', date('Y-m-d H:i:s'));
        }
    }

    public function afterSave(Event $event, EntityInterface $entity, ArrayObject $options)
    {
        $event = new Event('User.Model.Users.afterSave', $this, [$entity]);
        $this->getEventManager()->dispatch($event);
    }

    /**
     * Construye la query para que el usuario sea guardado en la sessión
     *
     * @param Query $query
     * @return void
     */
    public function findAuth(Query $query)
    {
        $query->contain('Groups');

        // Lanza el evento
        $event = new Event('User.Model.Users.findAuth', $this, [$query]);
        $this->getEventManager()->dispatch($event);

        return $query;
    }

    public function findActive(Query $query)
    {
        return $query->where([
            'Users.status' => 'active'
        ]);
    }

    public function findWithpassword(Query $query)
    {
        return $query->where([
            'Users.password is NOT NULL'
        ]);
    }

    public function getSocial($user, $provider)
    {
        $user = $this->find('auth')
            ->where([
                'Users.provider' => $provider,
                'Users.provider_uid' => $user['id']
            ])
            ->first();

        if ($user) {
            return $user->toArray();
        } else {
            return null;
        }
    }

    public function verifyPassword($user_id, $password)
    {
        $user = $this->get($user_id);

        if (!$user) {
            return false;
        }

        $data = [
            'email' => $user->email,
            'password' => $password,
        ];

        if (Configure::read('User.passwordHasher')) {
            $hasher = Configure::read('User.passwordHasher');
            $obj = new $hasher();
        } else {
            $obj = new DefaultPasswordHasher;
        }

        return $obj->check($password, $user->get('password'));
        $entity = $this->newEntity([
            'password' => $password
        ]);

        return $entity->password == $user->password;
    }

    /**
     * Se utiliza para que desde fuera se pueda validar los datos del usuario
     *
     * @param Validator $validator
     * @return void
     */
    public function validationCustom(Validator $validator)
    {
        $event = new Event('User.Model.Users.validationCustom', $this, [$validator]);
        $this->getEventManager()->dispatch($event);
        return $validator;
    }


    public function validationDefault(Validator $validator)
    {
        $validator
            ->notEmpty('email')
            ->add('email', 'validFormat', [
                'rule' => 'email',
                'message' => __d('app', 'El email ha de ser válido'),
            ])
            // ->notEmpty( 'username')
            // ->add( 'username', [
            //   'uniqueUser' => [
            //     'rule' => 'validateUniqueUsername', 
            //     'provider' => 'table',
            //     'message'=> __d( 'app', 'El nombre de usuario ya existe, por favor, elige otro'),
            //   ]
            // ])
        ;


        $event = new Event('User.Model.Users.validationDefault', $this, [$validator]);
        $this->getEventManager()->dispatch($event);

        return $validator;
    }

    public function setDeleted($user)
    {
        $this->query()->update()
            ->set([
                'status' => 'deleted'
            ])
            ->where([
                'id' => $user->id
            ])
            ->execute();
    }



    /**
     * validación formulario de login
     * @param  Validator $validator 
     * @return boolean               
     */
    public function validationLogin(Validator $validator)
    {
        return $validator
            ->notEmptyString('email', __d('app', 'El campo no puede estar vacio'))
            ->add('email', 'validFormat', [
                'rule' => 'email',
                'message' => __d('app', 'El email ha de ser válido'),
            ])
            ->notEmptyString('password', [
                'message' => __d('app', 'Este campo no puede estar vacío'),
            ])
            ->add('password', [
                'length' => [
                    'rule' => ['minLength', 5],
                    'message' => __d('app', 'La contraseña ha de tener 5 caracteres'),
                ]
            ]);
    }

    /**
     * validación formulacior de registro
     * @param  Validator $validator 
     * @return boolean               
     */
    public function validationRegister(Validator $validator)
    {
        $validator
            ->notEmptyString('name', 'El campo no puede estar vacio')
            ->notEmptyString('email', 'El campo no puede estar vacio')
            ->add('email', [
                'unique' => [
                    'rule' => 'validateUniqueEmail',
                    'provider' => 'table',
                    'message' => __d('app', 'El correo electrónico ya existe, por favor, elige otro'),
                ]
            ])
            ->add('email', 'validFormat', [
                'rule' => 'email',
                'message' => __d('app', 'El email ha de ser válido'),
            ])
            ->add('accept', 'notEmpty', [
                'rule' => ['notEmpty', true],
                'message' => __d('app', 'Es necesario aceptar las condiciones'),
            ])
            ->add('password', 'length', [
                'rule' => ['minLength', 5],
                'message' => __d('app', 'Las contraseñas han de tener como mínimo 5 carácteres'),
            ])
            ->add('password', [
                'custom' => [
                    'rule' => 'validatePassword',
                    'provider' => 'table',
                    'message' => __d('app', 'La contraseña debe estar formada por, como mínimo, por una letra y un número'),
                ],
            ])
            ->add('name', [
                'custom' => [
                    'rule' => 'validateNoUrlString',
                    'provider' => 'table',
                    'message' => __d('app', 'La contraseña no debe contener una URL'),
                ],
            ])
            ->add('password2', 'custom', [
                'rule' => function ($value, $context) {
                    if ($value !== $context['data']['password']) {
                        return false;
                    }
                    return true;
                },
                'message' => __d('app', 'Las contraseñas han de ser iguales'),
            ])
            ->add('lopd', 'custom', [
                'rule' => function ($value, $context) {
                    if ($value == '0') {
                        return false;
                    }
                    return true;
                },
                'message' => __d('app', "Debes aceptar el tratamiento de datos"),
            ]);


        $event = new Event('User.Model.Users.validationRegister', $this, [$validator]);
        $this->getEventManager()->dispatch($event);
        return $validator;
    }

    /**
     * validación formulacior de registro
     * @param  Validator $validator 
     * @return boolean               
     */
    public function validationRegisterShop(Validator $validator)
    {
        return $validator
            ->requirePresence('name', true, 'El campo no puede estar vacio')
            ->requirePresence('email', true, 'El campo no puede estar vacio')
            ->notEmptyString('name', 'El campo no puede estar vacio')
            ->notEmptyString('email', 'El campo no puede estar vacio')
            ->add('email', [
                'unique' => [
                    'rule' => 'validateUniqueEmail',
                    'provider' => 'table',
                    'message' => __d('users', 'El correo electrónico ya existe, por favor, elige otro'),
                ]
            ])
            ->add('email', 'validFormat', [
                'rule' => 'email',
                'message' => __d('users', 'El email ha de ser válido'),
            ])
            ->allowEmptyString('password')
            ->allowEmptyString('password2')
            ->add('password', 'length', [
                'rule' => ['minLength', 5],
                'message' => __d('app', 'Las contraseñas han de tener como mínimo 5 carácteres'),
            ])
            ->add('password', [
                'custom' => [
                    'rule' => 'validatePassword',
                    'provider' => 'table',
                    'message' => __d('app', 'La contraseña debe estar formada por, como mínimo, por una letra y un número'),
                ],
            ])
            ->notEmptyString('phone', __d('users', 'Este campo no puede estar vacío'))
            ->requirePresence('phone', true, __d('users', 'Este campo no puede estar vacío'))
            ->add('password2', 'custom', [
                'rule' => function ($value, $context) {
                    if ($value !== $context['data']['password']) {
                        return false;
                    }
                    return true;
                },
                'message' => __d('app', 'Las contraseñas han de ser iguales'),
            ]);
    }

    public function validationEdit(Validator $validator)
    {
        return $validator
            ->notEmptyString('name')
            ->notEmptyString('email')
            ->add('email', [
                'unique' => [
                    'rule' => 'validateUniqueEmail', 'provider' => 'table',
                    'message' => __d('app', 'El correo electrónico ya existe, por favor, elige otro'),
                ]
            ])
            ->add('username', [
                'unique' => [
                    'rule' => 'validateUniqueUsername', 'provider' => 'table',
                    'message' => __d('app', 'El nombre de usuario ya existe, por favor, elige otro'),
                ]
            ])
            ->add('current_password', 'verify', [
                'rule' => function ($value, $context) {
                    if (empty($value)) {
                        return true;
                    }

                    if (!empty($value)) {
                        return $this->verifyPassword($context['data']['id'], $value);
                    }
                },
                'message' => __d('app', 'La contraseña no es correcta'),
            ])
            ->notEmptyString('password', function($context) {
                return empty($context['data']['current_password']);
            })
            ->notEmptyString('password2')
            ->allowEmptyString('password', null, function($context) {
                return empty($context['data']['current_password']);
            })
            ->allowEmptyString('password2', null, function($context) {
                return empty($context['data']['current_password']);
            })
            ->allowEmptyString('current_password')
            ->add('password', [
                'custom' => [
                    'rule' => 'validatePassword',
                    'provider' => 'table',
                    'message' => __d('users', 'La contraseña debe estar formada por, como mínimo, por una letra y un número'),
                ],
            ])
            ->add('password2', 'custom', [
                'rule' => function ($value, $context) {


                    if ($value !== $context['data']['password']) {
                        return false;
                    }
                    return true;
                },
                'message' => __d('app', 'Las contraseñas han de ser iguales'),
            ]);
    }

    public function validationEditAdmin(Validator $validator)
    {
        return $validator
            ->notEmptyString('name',)
            ->notEmptyString('email')
            ->add('email', [
                'unique' => [
                    'rule' => 'validateUniqueEmail', 'provider' => 'table',
                    'message' => __d('app', 'El correo electrónico ya existe, por favor, elige otro'),
                ]
            ])
            ->allowEmptyString('current_password', null, function($context) {
                return empty($context['data']['change_password']);
            })
            ->add('current_password', 'verify', [
                'rule' => function ($value, $context) {
                   
                    if (empty($value)) {
                        return false;
                    }

                    if (!empty($value)) {
                        return $this->verifyPassword($context['data']['id'], $value);
                    }
                },
                'message' => __d('app', 'La contraseña no es correcta'),
            ])
            ->notEmptyString('password', null, function($context) {
                return empty($context['data']['current_password']);
            })
            ->notEmptyString('password2')
            ->add('password', [
                'custom' => [
                    'rule' => 'validatePassword',
                    'provider' => 'table',
                    'message' => __d('users', 'La contraseña debe estar formada por, como mínimo, por una letra y un número'),
                ],
            ])
            ->add('password2', 'custom', [
                'rule' => function ($value, $context) {


                    if ($value !== $context['data']['password']) {
                        return false;
                    }
                    return true;
                },
                'message' => __d('app', 'Las contraseñas han de ser iguales'),
            ]);
    }

    /**
     * validación formulario de recuperar contraseña
     * @param  Validator $validator 
     * @return boolean               
     */
    public function validationForgotpassword(Validator $validator)
    {
        return $validator
            ->notEmptyString('email')
            ->add('email', 'validFormat', [
                'rule' => 'email',
                'message' => __d('users', 'El email ha de ser válido'),
            ])
            ->add('email', 'exist', [
                'rule' => function ($value) {
                    return $this->exists([
                        'Users.email' => $value
                    ]);
                },
                'message' => __d('app', 'El email indicado no pertenece a ningún usuario'),
            ]);
    }

    /**
     * validación del cambio de contraseña
     * @param  Validator $validator [description]
     * @return [type]               [description]
     */
    public function validationNewpassword(Validator $validator)
    {
        return $validator
            ->notEmptyString('password')
            ->notEmptyString('password2')
            ->add('password', 'length', [
                'rule' => ['minLength', 5],
                'message' => __d('app', 'Las contraseñas han de tener como mínimo 5 carácteres'),
            ])
            ->add('password', [
                'custom' => [
                    'rule' => 'validatePassword',
                    'provider' => 'table',
                    'message' => __d('app', 'La contraseña debe estar formada por, como mínimo, una letra y un número'),
                ],
            ])
            ->add('password2', 'custom', [
                'rule' => function ($value, $context) {
                    if ($value !== $context['data']['password']) {
                        return false;
                    }
                    return true;
                },
                'message' => __d('app', 'Las contraseñas han de ser iguales'),
            ]);
    }

    public function validatePassword($value, $context)
    {
        $pattern = '/[A-Za-z].*[0-9]|[0-9].*[A-Za-z]/';
        return !empty($value) && (bool)preg_match($pattern, $value);
    }

    public function validateNoUrlString($value, $context)
    {
        return strpos($value, '://') === false;
    }

    public function validateUniqueEmail($value, $context)
    {
        if (array_key_exists('registered', $context['data']) && $context['data']['registered'] != 1) {
            return true;
        }

        $query = $this->find()
            ->where([
                'Users.email' => $value,
                'Users.registered' => true,
                // 'Users.status NOT IN' => [
                //   'inactive',
                //   'pending'
                // ],
            ])
            ->where([
                'OR' => [
                    'Users.status' => 'active',
                    'Users.status IS NULL'
                ]
            ]);


        if (array_key_exists('id', $context['data']) && !empty($context['data']['id'])) {
            $query->where([
                'Users.id !=' => $context['data']['id']
            ]);
        }

        return $query->count() == 0;
    }


    public function validateUniqueUsername($value, $context)
    {
        if (empty($value)) {
            return true;
        }

        $query = $this->find()
            ->where([
                'Users.username' => $value,
            ]);

        if (array_key_exists('id', $context['data']) && !empty($context['data']['id'])) {
            $query->where([
                'Users.id !=' => $context['data']['id']
            ]);
        }

        return $query->count() == 0;
    }

    // public function beforeSave( Event $event, Entity $entity)
    // {
    // }

    // public function afterSave( Event $event, Entity $entity)
    // {
    // }

    /**
     * comprueba si  el usuario está logueado
     * @param  object $user 
     * @return boolean       
     */
    public function ifuserloggedin($user)
    {
        if ($user != null)
            return true;
    }


    public function setDefaultGroup($user)
    {
        if (!empty($user->group_id)) {
            return;
        }

        $group = $this->Groups->find()->where([
            'Groups.by_default' => 1
        ])->first();

        $user->set('group_id', $group->id);
    }

    public function setDefaultStatus($activate, $user)
    {
        if ($activate == 'none') {
            $status = 'active';
        } else {
            $status = 'pending';
        }

        $user->set('status', $status);
    }

    public function prepareRegister($user)
    {
        $token = Text::uuid();
        $this->setUid($token, $user);
        $this->setDefaultGroup($user);

        if (Website::get('settings.users_logued_after_login')) {
            $user->status = 'active';
        }

        $user->set('salt_register', $this->createSaltConfirmEmail());
    }

    public function setUid($token, $user)
    {
        $user->set('uid', $token);
    }

    public function generateSendLink()
    {
        $token = Text::uuid();
        $hash = Security::hash($token, 'sha1', true);
        $link = Router::url(array(
            'plugin' => 'User',
            'controller' => 'Users',
            'action' => 'confirm_register',
            $hash,
            $token
        ), true);

        return $link;
    }


    public function createSaltConfirmEmail()
    {
        return Security::hash(Text::uuid(), 'sha1', true);
    }

    public function resetConfirmEmail($user)
    {
        $user->set([
            'salt_register' => $this->createSaltConfirmEmail(),
        ]);

        return $this->save($user);
    }

    public function resetSaltForgot($user)
    {
        $user->set([
            'salt_forgot' => null,
        ]);

        return $this->save($user);
    }

    public function isEmailActivated($user)
    {
        $result = (object)[
            'isActivated' => false
        ];

        if (!Configure::read('User.confirmEmail')) {
            $result->isActivated = true;
        }

        if (empty($user['confirm_email_request_date'])) {
            $result->isActivated = true;
        }

        if (Configure::read('User.confirmEmail') && $user['confirm_email_request_date']) {
            $created = (int)$user['confirm_email_request_date']->toUnixString();
            $confirmTime = Configure::read('User.confirmEmailTimeAlive');

            if (!$confirmTime || ($created + $confirmTime) < time()) {
                $result->isActivated = false;
                $event = new Event('User.Model.Users.notConfirmedEmail', $this);
                $this->getEventManager()->dispatch($event);
            } else {
                $result->isActivated = true;
            }
        }

        if ($user['confirmed_email']) {
            $result->isActivated = true;
        }

        $event = new Event('User.Model.Users.isActivated', $this, [$result]);
        $this->getEventManager()->dispatch($event);
        return $result->isActivated;
    }

    public function updateLastLogin($id)
    {
        $user = $this->find()->where(['id' => $id])->first();
        $user->set('last_login', date('Y-m-d H:i:s'));
        $this->save($user);
    }

    public function setActivatedEmail($user)
    {
        $user->set('salt_register', null);
        $user->set('confirmed_email', true);
        $user->set('confirm_email_request_date', date('Y-m-d H:i:s'));
        $this->save($user);

        $event = new Event('User.Model.Users.setActivatedEmail', $this, [$user]);
        $this->getEventManager()->dispatch($event);
    }

    public function generateForgotSalt($user)
    {
        $salt = Security::hash($user->id . time());
        $user->salt_forgot = $salt;
        $this->save($user);
        return $salt;
    }
}
