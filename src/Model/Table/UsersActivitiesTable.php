<?php

namespace User\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\Entity;
use Cake\Event\Event;
use Cake\Database\Schema\TableSchema;

// use Acl\Model\Behavior\AclBehavior;
/**
 * Users Model
 */
class UsersActivitiesTable extends Table
{

  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
    $this->setTable('users_activities');
    $this->setPrimaryKey('id');
    $this->addBehavior('Timestamp', [
      'events' => [
        'Model.beforeSave' => [
          'date' => 'always',
        ]
      ],
    ]);

    // $this->belongsTo('Users', [
    //   'foreignKey' => 'user_id',
    //   'className' => 'User.Users',
    // ]);
  }

  protected function _initializeSchema(TableSchema $schema)
  {
    $schema->setColumnType('data', 'json');
    $schema->setColumnType('query', 'json');
    return $schema;
  }
}
