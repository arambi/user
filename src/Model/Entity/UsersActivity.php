<?php
namespace User\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * User Entity.
 */
class UsersActivity extends Entity {

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
  protected $_accessible = [
    'user_id' => true,
    'browser' => true,
    'language' => true,
    'domain' => true,
    'ip' => true,
    'request_method' => true,
    'url' => true,
    'status_code' => true,
    // 'date' => true
  ];


}
