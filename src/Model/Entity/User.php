<?php
namespace User\Model\Entity;

use I18n\Lib\Lang;
use Cake\ORM\Entity;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Event\EventManager;
use Section\Routing\RouteData;
use Cake\Auth\DefaultPasswordHasher;
use Manager\Model\Entity\CrudEntityTrait;

/**
 * User Entity.
 */
class User extends Entity {

	use CrudEntityTrait;

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
    'id' => false,
    '*' => true,
	];

	protected $_virtual = [
		'age',
		'locale_human'
  ];

  	protected function _getFullTitle()
	{
		return implode(' ', [$this->name, $this->lastname]);
	}


	protected function _setPassword( $password) 
	{
  	return $this->setPassword( $password);
  }

  protected function _setPassword2( $password2) 
	{
  	return $this->setPassword( $password2);
  }

  public function setPassword( $password)
  {
    if( Configure::read( 'User.passwordHasher'))
    {
      $hasher = Configure::read( 'User.passwordHasher');
      $obj = new $hasher();
      return $obj->hash( $password);
    }

		if (strlen($password) > 0) {
			return (new DefaultPasswordHasher)->hash($password);
		}
  }

  protected function _getAge()
  {
  	if( empty( $this->birthday))
  	{
  		return null;
  	}

  	//date in mm/dd/yyyy format; or it can be in other formats as well
	  $birthDate = date( 'm/d/Y', $this->birthday->toUnixString());
	  //explode the date to get month, day and year
	  $birthDate = explode("/", $birthDate);

	  //get age from date or birthdate
	  $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")
	    ? ((date("Y") - $birthDate[2]) - 1)
	    : (date("Y") - $birthDate[2]));

	 	return $age;
	}
	
	protected function _getLocaleHuman()
	{
		if( !empty( $this->locale))
		{
			return Lang::getName( $this->locale);
		}
	}

	protected function _getConfirmEmailUrl()
	{
		return RouteData::url([
			'plugin' => 'User',
			'controller' => 'Users',
			'action' => 'confirm_register',
		], true). "/{$this->salt}/{$this->salt_register}";
	}

	protected function _getSendConfirmEmailUrl()
	{
		return RouteData::url([
			'plugin' => 'User',
			'controller' => 'Users',
			'action' => 'confirmationLink',
		], true). "?send=1";
	}

	public function isActive()
	{
		$result = (object)[
			'active' => false
		];

		if ($this->status == 'active') {
			$this->active = true;
		}

		$event = new Event('User.Model.Users.userIsActive', $this, [$result]);
    EventManager::instance()->dispatch( $event);
		return $result->active;
	}
}
