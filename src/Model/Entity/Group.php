<?php
namespace User\Model\Entity;

use Cake\ORM\Entity;
use Manager\Model\Entity\CrudEntityTrait;
use Cake\ORM\Behavior\Translate\TranslateTrait;

/**
 * Group Entity.
 */
class Group extends Entity 
{
  use CrudEntityTrait;
  use TranslateTrait;

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
    '*' => true,
	];

  protected $_virtual = [
    'full_title'
  ];
}
