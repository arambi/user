<?php
namespace User\Model\Entity;

use Cake\ORM\Entity;

/**
 * Invitation Entity.
 */
class Invitation extends Entity {

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
		'user_id' => true,
		'new_user_id' => true,
		'name' => true,
		'email' => true,
		'body' => true,
		'salt' => true,
		'user' => true,
		'new_user' => true,
	];

}
