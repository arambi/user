<?php

namespace User\Auth;

use stdClass;
use Cake\Event\Event;
use User\Auth\Access;
use Cake\Network\Request;
use Cake\Network\Session;
use Cake\Utility\Inflector;
use Cake\Auth\BaseAuthorize;
use Cake\Event\EventManager;
use Cake\Http\ServerRequest;

class CrudAuthorize extends BaseAuthorize 
{
  /**
 * Get the action path for a given request. Primarily used by authorize objects
 * that need to get information about the plugin, controller, and action being invoked.
 *
 * @param \Cake\Network\Request $request The request a path is needed for.
 * @param string $path Path
 * @return string The action path for the given request.
 */
  public function action(ServerRequest $request, $path = '/:plugin/:controller/:action') 
  {
    $plugin = empty($request['plugin']) ? null : Inflector::camelize($request['plugin']) . '/';
    $path = str_replace(
      array(':controller', ':action', ':plugin/'),
      array(Inflector::camelize($request['controller']), $request['action'], $plugin),
      $path
    );
    $path = str_replace('//', '/', $path);
    return trim($path, '/');
  }

  public function authorize($user, ServerRequest $request) 
  {
    $section = $request->getParam( 'section');

    if( $section)
    {
      $access = new stdClass();

      if( $section->restricted && !empty( $section->groups)) {
        $groups = collection( $section->groups)->extract( 'id')->toArray();
        $access->access = in_array( $user ['group_id'], $groups);
      }

      if( $section->no_access_group && !empty( $section->no_groups)) {
        $groups = collection( $section->no_groups)->extract( 'id')->toArray();
        $access->access = !in_array( $user ['group_id'], $groups);
      }

      $event = new Event( 'Section.Section.hasPermissions', $this, [$access, $section, $user]);
      EventManager::instance()->dispatch( $event);
      return $access->access;
    }

    return Access::check( $user ['group_id'], $request);
  }
}