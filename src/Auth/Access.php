<?php

namespace User\Auth;

use Cake\Core\Plugin;
use Cake\Core\Configure;
use Cake\Utility\Inflector;
use Cake\Utility\Hash;
use Cake\ORM\TableRegistry;
use Cake\Network\Request;


class Access
{
    protected static $_isLoaded = false;

    /**
     * Aquí se va guardando la configuración de cada Plugin
     */
    protected static $_config = [];

    protected static $_nodes = ['master' => []];

    protected static $_allNodes = [];

    protected static $_master = 'master';

    protected static $_groupAccess = [];


    public static function add($key, $data)
    {
        self::$_config = array_merge(self::$_config, [$key => $data]);
        self::normalize();
    }

    /**
     * Lee la configuración de cada plugin
     *
     * @return void
     */
    public static function loadConfig()
    {
        self::normalize();
        self::$_isLoaded = true;
    }



    /**
     * [normalize description]
     * @return [type] [description]
     */
    public static function normalize()
    {
        foreach (self::$_config as $key => $options) {
            if ($key == self::$_master) {
                continue;
            }

            $nodes = [];

            foreach ($options['options'] as $option => $values) {
                $base = $values['nodes'][0];

                if (array_key_exists('prefix', $base) && $base['prefix'] == 'admin') {
                    foreach (['duplicate', 'bulk', 'field', 'sortable', 'autocomplete'] as $value) {
                        $base ['action'] = $value;
                        $values ['nodes'][] = $base;
                    }
                }

                foreach ($values['nodes'] as $node) {
                    $action = self::action($node);
                    $nodes[$option][] = $action;
                    self::$_allNodes[] = $action;
                }
            }

            self::$_nodes[self::$_master][$key] = $nodes;
        }
    }

    /**
     * Devuelve un valor de la configuración de las Section, dado un hash tipo Plugin.key1.key2
     *
     * @param string $var 
     * @return void
     * @example Access::read( 'Blog.name') => devolverá el nombre humano del plugin
     */
    public static function getNodes($var)
    {
        if (!self::$_isLoaded) {
            self::loadConfig();
        }
        $return = Hash::get(self::$_nodes, $var);

        if ($return === null) {
            return [];
        }

        return $return;
    }


    public static function appendNodes($path, $data)
    {
        $parts = explode('.', $path);
        $parent = self::$_config[$parts[0]]['options'][$parts[1]]['nodes'];

        self::$_config[$parts[0]]['options'][$parts[1]]['nodes'] = array_merge($parent, $data);
        self::normalize();
    }

    /**
     * Devuelve las opciones para ser tratadas en un formulario con checkboxes
     * En principio, es usado en el formulario de edición de grupos de usuario
     *
     * El valor devuelto es un array asociativo en forma de árbol con los siguientes keys
     * ´name´ El nombre humano del grupo de nodos
     * ´node´ El path del nodo tipo 'master.articles.update' o 'master.articles'
     * ´children´ Los nodos hijos
     *   
     * @return array
     */
    public static function getOptions()
    {
        // Lee la configuración, si ésta no existe
        if (!self::$_isLoaded) {
            self::loadConfig();
        }

        // Inicia el array con master
        $return = ['master' => [
            'name' => 'Master',
            'node' => 'master',
            'children' => []
        ]];

        // Recorre la configuración
        foreach (self::$_config as $key => $options) {
            $childrens = [];

            // Recorre las opciones
            foreach ($options['options'] as $option => $values) {
                // Va creando los hijos
                $childrens[] = [
                    'name' => $values['name'],
                    'node' => "master.$key.$option"
                ];
            }

            // Añade un nuevo nodo
            $return['master']['children'][] = [
                'name' => $options['name'],
                'node' => "master.$key",
                'children' => $childrens
            ];
        }

        return $return;
    }

    /**
     * a partir de un array de una URL de Cakephp, genera un string tipo /Prefix/Plugin/controller/action
     * @param  array $request El array del request
     * @param  string $path    Los valores del path a formar
     * @return string La URL solicitada, en formato string
     */
    public static function action($request, $path = '/:prefix/:plugin/:controller/:action')
    {
        // Si el prefix y el plugin no están definidos se pone a null
        $prefix = empty($request['prefix']) ? null : Inflector::camelize($request['prefix']) . '/';
        $plugin = empty($request['plugin']) ? null : Inflector::camelize($request['plugin']) . '/';

        // Sustitutye cada parámetro por su valor
        $path = str_replace(
            array(':controller', ':action', ':plugin', ':prefix'),
            array(Inflector::camelize($request['controller']), $request['action'], $plugin, $prefix),
            $path
        );

        // Elimina las barras dobles // por sencillas /
        // Estas barras son generadas debido a que puede que el plugin o el prefix no esté definido
        $path = str_replace('//', '/', str_replace('//', '/', $path));

        // Devuelve el string de la URL
        return trim($path, '/');
    }

    /**
     * Setea en self::$_groupAccess los nodos a los que tiene acceso un grupo
     * @param integer $group_id El id del grupo
     */
    public static function setGroupAccess($group_id)
    {
        // Toma el grupo
        $Groups = TableRegistry::getTableLocator()->get('User.Groups');
        $group = $Groups->findById($group_id)->first();
        $permissions = $group->permissions;

        if (!is_array($permissions)) {
            $permissions = [];
        }

        // Toma los permisos a los que tiene acceso el grupo
        foreach ($permissions as $permission) {
            $nodes = self::getNodes($permission);
            self::setGroupNodes($nodes);
        }
    }


    /**
     * Resetea el grupo de acceso para que vuelva a ser creado
     */
    public static function resetGroupAccess()
    {
        static::$_groupAccess = [];
    }

    /**
     * Setea los nodos en self::$_groupAccess recursivamente, para un grupo
     * @param void
     */
    public static function setGroupNodes($nodes)
    {
        foreach ($nodes as $key => $node) {
            if (is_array($node)) {
                self::setGroupNodes($node);
            } else {
                self::$_groupAccess[] = $node;
            }
        }
    }

    /**
     * Chequea que un grupo tenga acceso a un nodo
     * 
     * @param  integer $group_id El id de la tabla groups
     * @param  array $request  URL request
     * @return boolean True si el grupo tiene acceso
     * @throws \RuntimeException si la acción no está definida en ninguno de los ficheros access.php
     */
    public static function check($group_id, $request)
    {
        // Solicita la creación de $_groupAccess si no está definido
        if (empty(self::$_groupAccess)) {
            self::setGroupAccess($group_id);
        }

        // Construye el request en un string
        $action = self::action($request);

        // Comprueba si la acción está definida en los ficheros access.php
        if (!in_array($action, self::$_allNodes)) {
            throw new \RuntimeException(sprintf(
                'Access, the node "%s" is not defined, it is necessary to do so in the access.php files or give access to AuthComponent::allow()',
                $action
            ));
        }

        // Comprueba si la acción está dentro de las que el grupo tiene acceso
        return in_array($action, self::$_groupAccess);
    }

    public static function hasAdminPermissions($group_id)
    {
        $request = new Request([
            'params' => [
                'prefix' => 'admin',
                'plugin' => 'Manager',
                'controller' => 'Pages',
                'action' => 'home',
            ]
        ]);

        return self::check($group_id, $request);
    }
}
