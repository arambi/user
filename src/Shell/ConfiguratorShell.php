<?php
namespace User\Shell;

use Cake\Console\Shell;
use Cake\Core\Plugin;
use Cake\Filesystem\Folder;

/**
 * Realiza la instalación de la aplicación
 */
class ConfiguratorShell extends Shell
{

/**
 * Crea el fichero de configuración por defecto del plugin
 * 
 * @return void
 */
  public function main()
  {
    $data = "<?php

              use Cake\Core\Configure;

              \$config ['User'] = [
                'defaults' => [
                  'group' => 'admin', // grupo por defecto
                  'projectname' => 'development enviroment', // nombre del proyecto que aparecerá en el subject de los emails
                  'url_after_login' => '/admin', // url donde redirige después de hacer login
                ],
                'recaptcha' => [
                  'active' => true,
                  'actions' => [
                    'register',
                  ]
                ]
              ];";

    // Creación de ficheros de configuración
    $this->writefile( $data );
  }

  public function writefile( $data )
  {
    $file = fopen( "config/user.php", "wb");
    fwrite( $file, $data);
    fclose( $file);
    $this->out( 'Configuración para el plugin User creada');
  }

}