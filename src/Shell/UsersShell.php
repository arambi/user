<?php
namespace User\Shell;

use Cake\Console\Shell;
use Cake\Utility\Text;

/**
 * Users shell command.
 */
class UsersShell extends Shell {


  public function initialize()
  {
    $this->loadModel( 'User.Groups');
    $this->loadModel( 'User.Users');
  }

  public function addGroup()
  {
    $data = array();
    $data ['name'] = $this->in( 'Introduce el nombre del grupo');
    
    if( empty( $data ['name']))
    {
      $this->out( 'Es necesario indicar el nombre del grupo. Bye.');
      die();
    }
    
    $group = $this->Groups->newEntity( $data);

    if( $this->Groups->save( $group))
    {
      $this->out( 'Se ha creado el grupo <'. $data ['name'] .'> con id <'. $group->id .'>');
    }
    else
    {
      $this->out( 'No ha sido posible guardar el grupo');
    }
  }

  public function addUser()
  {
    $data = array();
    
    $groups = $this->Groups->find()->toArray();
    
    
    $save = array();

    foreach( $groups as $group)
    {
      $this->out( "{$group->id}. {$group->name}");
    }
    
    $group_id = $this->in( 'Selecciona un grupo para el usuario', null, $groups [0]->id);
    
    if( empty( $group_id) || !$this->Groups->find()->where(['id' => $group_id])->first())
    {
      $this->out( 'Es necesario indicar un grupo correcto. Bye.');
      die();
    }
    
    $save ['group_id'] = $group_id;
    $ins = array(
        'email' => 'Email',
        'password' => 'Contraseña',
        'password2' => 'Repite la contraseña',  
        'name' => 'Nombre'
    );   
    
    foreach( $ins as $key => $value)
    {
      $data = $this->in( "Indica un $value");
      
      if( empty( $data))
      {
        $this->out( "Es necesario indicar un $value Bye.");
        die();
      }

      $save [$key] = $data;
    }
    
    $save ['status'] = 'active';
    $save ['uid'] = Text::uuid();
    
    $user = $this->Users->newEntity( $save);

    if( $this->Users->save( $user))
    {
      $this->out( 'Se ha creado el usuario <'. $user->email .'> con id <'. $user->id .'>');
    }
    else
    {
      $this->out( 'No ha sido posible guardar el usuario');
    }
  }

}
