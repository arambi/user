<?php
namespace User\Shell;

use Cake\Console\Shell;
use Cake\ORM\TableRegistry;
use Cake\Core\Plugin;
use Cake\Utility\Text;

/**
 * Dumper shell command.
 */
class DumperShell extends Shell
{

  /**
   * main() method.
   *
   * @return bool|int Success or error code.
   */
  public function main() 
  {
    $this->Groups = TableRegistry::get( 'User.Groups');
    $this->Users = TableRegistry::get( 'User.Users');
    $this->createGroups();
    $this->createUser();
  }

  private function __getData( $file)
  {
    $path = Plugin::path( 'User') . 'src' .DS. 'Model' .DS. 'Data' .DS;
    $content = file_get_contents( $path . $file .'.json');
    return json_decode( $content, true);
  }

  public function createGroups()
  {
    if( $this->Groups->find()->count() > 0)
    {
      return;
    }

    $data = $this->__getData( 'groups');

    foreach( $data ['groups'] as $record)
    {
      $entity = $this->Groups->getNewEntity( $record);
      $this->Groups->save( $entity);
    }
  }

  public function createUser()
  {
    $this->Groups = TableRegistry::get( 'User.Groups');
    $this->Users = TableRegistry::get( 'User.Users');

    $data = array();
    
    $groups = $this->Groups->find()->toArray();
    
    $save = array();

    foreach( $groups as $group)
    {
      $this->out( "{$group->id}. {$group->name}");
    }
    
    $group_id = $this->in( 'Selecciona un grupo para el usuario', null, $groups [0]->id);
    
    if( empty( $group_id) || !$this->Groups->find()->where(['id' => $group_id])->first())
    {
      $this->out( 'Es necesario indicar un grupo correcto. Bye.');
      die();
    }
    
    
    $ins = array(
        'email' => [
          'name' => 'Email',
          'default' => 'funtsak@funtsak.com'
        ],
        'password' => [
          'name' => 'Contraseña',
          'default' => 'niuShakatak0'
        ],
        'name' => [
          'name' => 'Nombre',
          'default' => 'Funtsak'
        ],
    );   
    
    foreach( $ins as $key => $values)
    {
      $data = $this->in( "Indica un {$values ['name']}", null, $values ['default']);
      
      if( empty( $data))
      {
        $this->out( "Es necesario indicar un {$values ['name']} Bye.");
        die();
      }

      $save [$key] = $data;
    }
    
    $save ['status'] = 'active';
    $save ['uid'] = Text::uuid();
    $save ['password2'] = $save ['password'];

    $user = $this->Users->newEntity( $save);

    $user->group = $this->Groups->find()->where(['Groups.id' => $group_id])->first();

    if( $this->Users->save( $user))
    {
      $this->out( 'Se ha creado el usuario <'. $user->email .'> con id <'. $user->id .'>');
    }
    else
    {
      $this->out( 'No ha sido posible guardar el usuario');
    }

    $other = $this->in( '¿Crear otro usuario?', ['s', 'n'], 's');
    
    if( $other == 's')
    {
      $this->createUser();
    }
  }

  public function createUsers()
  {
    $Users = TableRegistry::get( 'User.Users');
    $data = $this->__getData( 'users');

    foreach( $data ['users'] as $record)
    {
      $entity = $Users->newEntity( $record);
      $Users->save( $entity);
    }
  }

}