<?php

namespace User\Mailer;

use ArrayObject;
use Cake\Event\Event;
use Cake\Mailer\Mailer;
use Cake\Routing\Router;
use Website\Lib\Website;
use Cake\Event\EventManager;
use Letter\Mailer\LetterTrait;
use Letter\Mailer\QueueMailer;
use Section\Routing\RouteData;
use Letter\Collector\LetterCollector;

/**
 * Correos electrónicos enviados para temas de usuarios de administración
 */

class UserAdminMailer extends QueueMailer
{
    use LetterTrait;

    public function forgot_password($user, $link)
    {
        $this->_email
            ->setTransport('default')
            ->setSubject(__d('admin', 'Restablecer contraseña en {0}', [Website::get('title')]))
            ->setFrom(Website::get('settings.users_reply_email'), Website::get('title'));

        $this->_email
            ->setEmailFormat('html')
            ->setTo($user->email)
            ->viewBuilder()->setTemplate('User.UsersAdmin/forgot_password');
        $this->_email
            ->set(compact('user', 'link'));
    }

    public function postRegisterFromAdmin($user)
    {
        $this->_email->setSubject(__d('admin', 'Registro completado en {0}', [
            Website::get('title')
        ]));

        $this->_email->setFrom(Website::get('settings.users_reply_email'), Website::get('title'));
        $this->_email->setEmailFormat('html');
        $this->_email->viewBuilder()->setTemplate('User.Users/post_register_from_admin', 'default');
        $this->_email
            ->setViewVars([
                'web_name' => Website::get('title'),
                'name' => $user->name,
                'email' => $user->email,
                'domain' => $_SERVER['HTTP_HOST'],
                'domain_url' => $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST']
            ])
            ->setTo($user->email);
    }

    public function url($url)
    {
        $section = RouteData::getSection($url);

        if ($section && is_object($section['section'])) {
            $route = $url;

            if (empty($section['section']->sectionType->actions)) {
                $route['section_id'] = $section['section']->id;
            }

            return Router::url($route);
        }

        return $url;
    }


    public function registerFromAdmin($user)
    {
        $this->_email->setSubject(__d('admin', 'Bienvenido a {0}', [
            Website::get('title')
        ]));

        $is_admin = (in_array('master', (array)$user->group->permissions) || in_array('master.admin', (array)$user->group->permissions));

        $url = $this->url([
            'prefix' => $is_admin ? 'admin' : false,
            'plugin' => 'User',
            'controller' => 'Users',
            'action' => 'complete',
        ]);

        $url = Router::url($url, true) . '/' . $user->salt;

        if ($is_admin) {
            $this->_email->setFrom(Website::get('settings.users_reply_email'), Website::get('title'));
            $this->_email->setEmailFormat('html');
            $this->_email->viewBuilder()->setTemplate('User.Users/register_from_admin', 'default');
            $this->_email
                ->setViewVars([
                    'web_name' => Website::get('title'),
                    'url' => $url,
                    'name' => $user->name,
                    'email' => $user->email,
                    'domain' => $_SERVER['HTTP_HOST'],
                    'domain_url' => $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST']
                ])
                ->setTo($user->email);
        } else {
            $data = new ArrayObject([
                'web_name' => Website::get('title'),
                'url' => $url,
                'name' => $user->name,
                'email' => $user->email,
                'domain' => $_SERVER['HTTP_HOST'],
                'domain_url' => $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST']
            ]);

            $event = new Event('User.Mailer.UserAdmin.registerData', $this, [$user, $data]);
            EventManager::instance()->dispatch($event);
                \Cake\Log\Log::debug( (array)$data);
            $this->letter = LetterCollector::get('User.complete_register', (array)$data);

            $this->_setLetterParams();

            $this->_email->setFrom(Website::get('settings.users_reply_email'), Website::get('title'));
            $this->_email->setTo($user->email);
        }
    }
}
