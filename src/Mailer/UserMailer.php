<?php

namespace User\Mailer;

use Cake\Mailer\Mailer;
use Website\Lib\Website;
use Letter\Collector\LetterCollector;
use Letter\Mailer\LetterTrait;
use Letter\Mailer\QueueMailer;
use Cake\Event\Event;
use Cake\Event\EventManager;

/**
 * Correos electrónicos enviados para acciones generales de usuarios
 * Aquí estará solo aquellas que no sean particulares de otros plugins
 */

class UserMailer extends QueueMailer
{
    use LetterTrait;


    public function register($user)
    {
        $this->letter = LetterCollector::get('User.register', [
            'web_name' => Website::get('title'),
            'name' => $user->name,
            'email' => $user->email,
            'username' => $user->username,
        ]);

        $this->_setLetterParams();

        $this->_email->setFrom(Website::get('settings.users_reply_email'), Website::get('title'));
        $this->_email->setTo($user->email);
    }


    public function forgot_password($user, $link)
    {
        $this->letter = LetterCollector::get('User.forgot_password', [
            'shop_name' => Website::get('title'),
            'web_name' => Website::get('title'),
            'url' => $link,
            'name' => $user->name,
            'username' => $user->username,
        ]);

        $this->_setLetterParams();

        $this->_email->setFrom(Website::get('settings.users_reply_email'), Website::get('title'));
        $this->_email->setTo($user->email);
    }

    public function newUser($user)
    {
        $this->letter = LetterCollector::get('User.new_user', [
            'name' => $user->name,
            'email' => $user->email,
            'confirm_url' => $user->confirm_email_url
        ]);

        $event = new Event('User.Mailer.Users.newUser', $this, [$user, $this->letter]);
        EventManager::instance()->dispatch($event);

        $this->_setLetterParams();
        $this->_email->setFrom($user->email);

        $to = Website::get('settings.new_users_notice_email');

        if (empty($to)) {
            $to = Website::get('settings.users_reply_email');
        }

        $this->_email->setTo($to);
    }



    public function confirmationLink($user, $url = null)
    {
        if (!$url) {
            $url = $user->confirm_email_url;
        }

        $this->letter = LetterCollector::get('User.confirmation_link', [
            'name' => $user->name,
            'email' => $user->email,
            'confirm_url' => $url
        ]);

        $this->_setLetterParams();
        $this->_email->setFrom(Website::get('settings.users_reply_email'));
        $this->_email->setTo($user->email);
    }
}
