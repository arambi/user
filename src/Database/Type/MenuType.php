<?php

namespace User\Database\Type;

use Cake\Database\Driver;
use Cake\Database\Type;
use Cake\Database\TypeInterface;
use InvalidArgumentException;
use JsonSerializable;
use PDO;
use Manager\Navigation\NavigationCollection;

/**
 * Json type converter.
 *
 * Use to convert json data between PHP and the database types.
 */
class MenuType extends Type implements TypeInterface
{

  public function toDatabase($value, Driver $driver)
  {
    if (is_resource($value)) 
    {
      throw new InvalidArgumentException('Cannot convert a resource value to JSON');
    }

    $return = [];

    foreach( (array)$value as $data)
    {
      $return [] = [
        'label' => $data ['label'],
        'icon' => isset( $data ['icon']) ? $data ['icon'] : '',
        'key' => $data ['key'],
        'nodes' => $data ['nodes'],
        'type' => isset( $data ['type']) ? $data ['type'] : 'item'
      ];
    }

    return json_encode( $return);
  }

  public function toPHP( $value, Driver $driver)
  {
    $values = json_decode( $value, true);
    return static::build( $values);
  }

  public static function build( $values)
  {
    $return = [];

    foreach( (array)$values as $val)
    {
      $data = NavigationCollection::get( $val ['key']);

      if( $data)
      {
        $val = array_merge( $data->setUrl()->toArray(), $val);
      }

      if( !empty( $val ['nodes']))
      {
        $val ['nodes']= static::build( $val ['nodes']);
      }

      $return [] = $val;
    }

    return $return;
  }

  /**
   * Get the correct PDO binding type for string data.
   *
   * @param mixed $value The value being bound.
   * @param \Cake\Database\Driver $driver The driver.
   * @return int
   */
  public function toStatement($value, Driver $driver)
  {
    return PDO::PARAM_STR;
  }


  public function marshal($value)
  {
    return $value;
  }
}
