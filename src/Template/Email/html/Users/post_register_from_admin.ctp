<table style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" align="center"
  width="100%" border="0" cellspacing="0" cellpadding="0">

  <!-- Content -->
  <tr>
    <td
      style="font-family: 'Muli-ExtraLight', sans-serif; letter-spacing: 0; font-weight:lighter; color:#404040; font-size:28px; line-height:27px;"
      align="left">
      Registro completado
    </td>
  </tr>
  <!-- End Content -->

  <tr>
    <td style="font-size:2px; line-height:2px;" height="12">&nbsp;</td>
  </tr>

  <!-- Content -->
  <tr>
    <td style="font-family: 'Muli-Light', sans-serif; color:#404040;  line-height:1.5;"
      align="left">
      ¡Felicidades! Has completado tu registro para administrar tu sitio web. Aquí tienes información importante. Guarda este correo electrónico para que puedas consultarlo después.<br /><br />
      <span style="font-family: 'Muli-Bold', sans-serif; ">TU SITIO WEB:<br />
        <a href="

<?= $domain ?>" style="color:#404040; text-decoration: none;"
          target="_blank"><?= $domain_url ?></a><br /><br />
        TU DIRECCIÓN DE CORREO ELECTRÓNICO PARA INICIAR SESIÓN:<br />
        <?= $email ?></span><br /><br />
        DIRECCIÓN PARA ADMINISTRAR TU SITIO WEB:<br />
        <a href="<?= $domain ?>/admin" style="color:#404040; text-decoration: none;"
          target="_blank"><?= $domain_url ?>/admin</a></span><br />

    </td>
  </tr>
  <!-- End Content -->

  <tr>
    <td style="font-size:2px; line-height:2px;" height="50">&nbsp;</td>
  </tr>
  <!-- Content -->
  <tr>
    <td
      style="font-family: 'Muli-Regular', sans-serif; font-weight:400; color:#fff;  line-height:25px;"
      align="center" class="elius_button1">
      <a href="<?= $domain ?>/admin" class="elius_button1_tbl"
        style="display: inline-block; background: #404040; padding: 20px 60px; color: #fff; letter-spacing: 1.75px;"
        target="_blank">ACCESO ADMIN</a>
    </td>
  </tr>
  <!-- End Content -->

  <tr>
    <td style="font-size:2px; line-height:2px;" height="50">&nbsp;</td>
  </tr>
  <!-- Content -->
  <tr>
    <td style="font-family: 'Muli-Light', sans-serif; color:#404040;  line-height:1.5;" align="left">
    <?php if( !empty(  \Manager\Lib\Partner::get( 'email_support'))): ?>
      Si tienes alguna duda, puedes contactar con nuestro Servicio técnico en <a href="mailto:<?= \Manager\Lib\Partner::get( 'email_support') ?>"><?= \Manager\Lib\Partner::get( 'email_support') ?></a>. Contestaremos a la mayor brevedad posible.
    <?php endif ?>
    </td>
  </tr>
  <!-- End Content -->

  <tr>
    <td style="font-size:2px; line-height:2px;" height="10">&nbsp;</td>
  </tr>

</table>