<p><?= $user->name ?>, <?= __d( 'email', 'muchas por registrate en la aplicación ') ?> <?= $projectname?>.</p>

<p><?= __d( 'email', 'Para activar tu usuario haz click en el siguiente enlace') ?>:</p>

<a href="<?= $link ?>" target="_blank"><?= $link ?></a>