<p><strong><?= __d( 'admin', 'Reestablecer contraseña en {0}', [\Website\Lib\Website::get( 'title')]) ?></strong></p>

<p><?= $user->name ?>, <?= __d( 'admin', 'para reestablecer tu contraseña haz click en el siguiente enlace') ?>:</p>

<a href="<?= $link ?>" target="_blank"><?= __d( 'admin', 'Recuperar contraseña') ?></a>

