<p><strong><?= __d( 'email', 'Reestablecer contraseña en ') ?> <?=  ?>.</strong></p>

<p><?= $user->name ?>, <?= __d( 'email', 'para reestablecer tu contraseña haz click en el siguiente enlace') ?>:</p>

<a href="<?= $link ?>" target="_blank"><?= __d( 'admin', 'Recuperar contraseña') ?></a>
