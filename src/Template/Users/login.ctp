<h3><?= __d( 'user', "Entrada") ?></h3>
<?= $this->Form->create( null, [
  'class' => 'm-t'
]) ?>
  <?= $this->Form->input( 'email', array(
      'label' => false,
      'class' => 'form-control',
      'placeholder' => __d( 'user', "Email"),
      'type' => 'text',
      'div' => array(
          'class' => 'form-group'
      )
  ))?>

  <?= $this->Form->input( 'password', array(
      'label' => false,
      'class' => 'form-control',
      'placeholder' => __d( 'user', "Contraseña"),
      'type' => 'password',
      'div' => array(
          'class' => 'form-group'
      )
  ))?>
  <button type="submit" class="btn btn-primary block full-width m-b"><?= __d( 'user', 'Entrar') ?></button>
  <a href="<?= $this->Url->build( array(
      'plugin' => 'User',
      'controller' => 'users',
      'action' => 'forgot_password'
    )) ?>">
    <?= __d( 'user', "He olvidado mi contraseña")?>
  </a>
  <a href="<?= $this->Url->build( array(
      //'prefix' => 'admin',
      'plugin' => 'User',
      'controller' => 'Users',
      'action' => 'register'
    )) ?>">
    <?= __d( 'user', "Registrarme")?>
  </a>
<?= $this->Form->end() ?>