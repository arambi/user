<h3><?= __d( 'user', "Cambiar contraseña") ?></h3>

<?= $this->Form->create( $user, [
  'context' => ['validator' => 'newpassword'],
  'class' => 'm-t'
]) ?>

    <?= $this->Form->input( 'Users.password', [
        'label' => __d( 'user', 'Introduce la nueva contraseña'),
        'value' => ''
      ]) ?>
    
    <?= $this->Form->input( 'Users.password2', [
      'label' => __d( 'user', 'Repite la nueva contraseña'),
      'type' => 'password',
      'value' => ''
    ]) ?>

  <button type="submit" class="btn btn-primary block full-width m-b"><?= __d( 'user', 'Guardar') ?></button>

<?= $this->Form->end() ?>