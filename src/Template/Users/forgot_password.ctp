<div class="crumbs row" id="default-crumbs">

    <a id="nav-open" class="nav-button" href="#">
      <i class="fa fa-bars"></i><span class="none"><?= __d( 'app', 'Expandir menú') ?></span>
    </a>

    <?= $this->element( 'crumbs') ?>

</div><!-- .row -->

<div class="default-container login-container clearfix">

  <div class="row">

    <?php $this->loadHelper( 'Recaptcha.Recaptcha') ?>
   
    <div class="grid_12t">

      <h4 class="title-bordered">
        <?= __d( 'user', 'Recuperar contraseña') ?>
      </h4>
      
      <?= $this->Form->create( $user, [
        'context' => ['validator' => 'forgotpassword']
      ]) ?>
        <?= $this->Form->input( 'email', array(
            'label' => __d( 'user', "Introduce tu correo electrónico:"),
            'class' => 'form-control',
            'placeholder' => __d( 'user', "Email"),
            'type' => 'email',
            'type' => 'text',
            'div' => array(
                'class' => 'form-group'
            )
        ))?>

        <button type="submit" class="btn btn-icon btn-green"><i class="fa fa-lock"></i> <?= __d( 'user', 'Enviar') ?></button>
        
      <?= $this->Form->end() ?>
    </div>    
  </div><!-- .row -->
</div><!-- . -->
