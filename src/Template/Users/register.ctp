<?= $this->Form->create( $user, [
    'context' => ['validator' => 'register']
]); ?>

  <?= $this->Form->input( 'Users.name', [
    'label' => __d( 'user', 'Nombre'),
    'error' => ['Not long enough' => __('This is not long enough')]
  ]) ?>
  
  <?= $this->Form->input( 'Users.email', [
  'label' => __d( 'user', 'Email')
  ]) ?>
  
  <?= $this->Form->input( 'Users.password', [
    'label' => __d( 'user', 'Contraseña')
  ]) ?>
  
  <?= $this->Form->input( 'Users.password2', [
    'label' => __d( 'user', 'Repite la contraseña'),
    'type' => 'password'
  ]) ?>
  
  <?php if( $recaptcha == true ): ?>
    <?= $this->Recaptcha->display() ?>
  <?php endif ?>

  <?= $this->Form->button( 'Enviar') ?>
  
<?= $this->Form->end() ?>