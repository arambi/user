<?php use Cake\Core\Configure; ?>
<div class="loginColumns animated fadeInDown">
  <div class="row">
    <div class="col-md-12">
      <?php if( Configure::read( 'Admin.withLogo')): ?>
      <?php if( Configure::read( 'Admin.logoImage')): ?>
          <div class="login-logo"><img  src="/img/<?= Configure::read( 'Admin.logoImage') ?>" /></div>
      <?php else: ?>
          <div class="login-logo"><img  src="/img/logo-admin.png" /></div>
      <?php endif ?>
    <?php endif ?>
      <div class="login-explain">
        <h2 class="font-bold"><?= __d( 'admin', 'Registro') ?></h2>
        <p>
          <?= __d( 'admin', 'Para completar tu registro por favor rellena los siguientes datos') ?>
        </p>
        <p>
          <?= __d( 'admin', 'Una vez que termines el registro serás redirigido al administrador del web y recibirás un correo con las instrucciones para acceder.') ?>
        </p>
      </div>
    </div>
    <div class="col-md-12">
      <div class="ibox-content">

        <?= $this->Form->create( $user, [
            'context' => ['validator' => 'register']
        ]); ?>
          <?= $this->Form->hidden( 'Users.id') ?>
          <?= $this->Form->hidden( 'Users.salt') ?>
          <?= $this->Form->input( 'Users.name', [
            'label' => __d( 'user', 'Nombre'),
            'error' => ['Not long enough' => __('This is not long enough')],
            'div' => [
              'class' => 'form-group',
            ],
            'class' => 'form-control'
          ]) ?>
          <?= $this->Form->input( 'Users.email', [
            'label' => __d( 'user', 'Email'),
            'div' => [
              'class' => 'form-group',
            ],
            'class' => 'form-control'
          ]) ?>
              <?= $this->Form->input( 'Users.password', [
            'label' => __d( 'user', 'Contraseña'),
            'value' => '',
            'div' => [
              'class' => 'form-group',
            ],
            'class' => 'form-control'
          ]) ?>
          <?= $this->Form->input( 'Users.password2', [
            'label' => __d( 'user', 'Repite la contraseña'),
            'type' => 'password',
            'value' => '',
            'div' => [
              'class' => 'form-group',
            ],
            'class' => 'form-control'
          ]) ?>
          <?= $this->Form->button( __d( 'admin', 'Guardar'), [
            'class' => 'btn btn-primary block full-width m-b m-t',
            'div' => [
              'class' => 'form-group',
            ],
          ]) ?>
        <?= $this->Form->end() ?>
      </div>
    </div>
  </div>
  <hr/>
</div>
