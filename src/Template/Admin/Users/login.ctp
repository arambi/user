<?php use \Cake\Core\Configure; #$this->Flash->render() ?>
  <?php if( Configure::read( 'Admin.withLogo')): ?>
    <?php if( Configure::read( 'Admin.logoImage')): ?>
        <div class="login-logo"><img  src="/img/<?= Configure::read( 'Admin.logoImage') ?>" /></div>
    <?php else: ?>
        <div class="login-logo"><a target="_blank" href="/"><img src="/img/logo-admin.png" /></a></div>
    <?php endif ?>
  <?php endif ?>
  <?php $this->loadHelper( 'Form', [
    // 'templates' => 'Manager.app_form.php'
  ]) ?>

  <?= $this->Form->create( $user, [
    'class' => 'm-t'
  ]) ?>
    <?= $this->Form->input( 'email', array(
        'label' => false,
        'placeholder' => __d( 'admin', "Email"),
        'type' => 'text',
    ))?>

    <?= $this->Form->input( 'password', array(
        'label' => false,
        'placeholder' => __d( 'admin', "Contraseña"),
        'type' => 'password',
    ))?>
    <button type="submit" class="btn btn-primary block full-width m-b"><?= __d( 'admin', 'Entrar') ?></button>
    <a href="<?= $this->Url->build( array(
        'prefix' => 'admin',
        'plugin' => 'User',
        'controller' => 'users',
        'action' => 'forgot_password'
      )) ?>">
      <?= __d( 'admin', "He olvidado mi contraseña")?>
    </a>
  <?= $this->Form->end() ?>