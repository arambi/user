<?php use \Cake\Core\Configure; $this->Flash->render() ?>
  
  <?php if( Configure::read( 'Admin.withLogo')): ?>
    <?php if( Configure::read( 'Admin.logoImage')): ?>
        <div class="login-logo"><img  src="/img/<?= Configure::read( 'Admin.logoImage') ?>" /></div>
    <?php else: ?>
        <div class="login-logo"><img  src="/img/logo-admin.png" /></div>
    <?php endif ?>
  <?php endif ?>
      <div class="login-explain">
      <h2 class="font-bold"><?= __d( 'admin', 'Reestablecer contraseña') ?></h2>
        <p>
          <?= __d( 'admin', 'Introduce la nueva contraseña para el acceso.') ?>
        </p>
      </div>
        <?= $this->Form->create( $user, [
            'context' => ['validator' => 'newpassword']
        ]); ?>
          <?= $this->Form->input( 'Users.password', [
              'label' => __d( 'user', 'Introduce la nueva contraseña'),
              'class' => 'form-control',
              'value' => '',
              'div' => array(
                  'class' => 'form-group'
              )
            ]) ?>
          
          <?= $this->Form->input( 'Users.password2', [
            'label' => __d( 'user', 'Repite la nueva contraseña'),
            'class' => 'form-control',
            'type' => 'password',
            'value' => '',
            'div' => array(
                  'class' => 'form-group'
              )
          ]) ?>
          <?= $this->Form->button( __d( 'admin', 'Restablecer contraseña'), [
            'class' => 'btn btn-primary block full-width m-b m-t',
            'div' => [
              'class' => 'form-group',
            ],
          ]) ?>
        <?= $this->Form->end() ?>
