<?php use \Cake\Core\Configure; $this->Flash->render() ?>
  <?php if( Configure::read( 'Admin.withLogo')): ?>
    <?php if( Configure::read( 'Admin.logoImage')): ?>
        <div class="login-logo"><img  src="/img/<?= Configure::read( 'Admin.logoImage') ?>" /></div>
    <?php else: ?>
        <div class="login-logo"><img  src="/img/logo-admin.png" /></div>
    <?php endif ?>
  <?php endif ?>
    <div class="login-explain">
      <h2 class="font-bold"><?= __d( 'admin', 'Contraseña olvidada') ?></h2>
      <p><?= __d( 'admin', 'Si has olvidado la contraseña, envíamos tu email y recibirás un correo con las instrucciones para cambiarla') ?></p>
    </div>
        <?= $this->Form->create( $user, [
            'context' => ['validator' => 'register']
        ]); ?>
        <?= $this->Form->hidden( 'Users.id') ?>
        <?= $this->Form->hidden( 'Users.salt') ?>
        <?= $this->Form->input( 'email', array(
            'label' => __d( 'user', "Introduce tu correo electrónico:"),
            'placeholder' => __d( 'user', "Email"),
            'type' => 'email',
            'div' => array(
                'class' => 'form-group'
            )
        ))?>
        <?= $this->Form->button( __d( 'admin', 'Enviar'), [
          'class' => 'btn btn-primary block full-width m-b m-t',
          'div' => [
            'class' => 'form-group',
          ],
        ]) ?>
      <?= $this->Form->end() ?>
