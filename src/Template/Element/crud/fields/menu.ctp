

<div class="wrapper wrapper-content animated fadeIn" ng-controller="AdminMenuCtrl">
  <div class="row">
    <div class="col-md-6">
      <div class="ibox">
        <div class="ibox-title">
          <h5><?= __d( 'admin', 'Elementos disponibles') ?></h5>
        </div>
        <div class="ibox-content">
          <div ui-tree="treeOptions" data-clone-enabled="true" data-nodrop-enabled="true"  class="menu-edition-list" id="tree-root-elements">
            <ol class="dd-list" ui-tree-nodes="" ng-model="separators">
              <li class="dd-item" ng-repeat="el in separators" ui-tree-node><div ui-tree-handle class="dd-handle tree-node tree-node-content"><i class="fa fa-bars"></i> <?= __d( 'admin', 'Separador') ?></div></li>
            </ol>
          </div>
          <div ui-tree="treeOptions" data-clone-enabled="true" data-nodrop-enabled="true"  class="menu-edition-list" id="tree-root-elements">
            <ol class="dd-list" ui-tree-nodes="" ng-model="field.options">
              <li class="dd-item" class="handle" ng-repeat="el in field.options" ui-tree-node><div ui-tree-handle class="dd-handle tree-node tree-node-content"><i class="{{ el.icon }}"></i> {{ el.label }}</div></li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="ibox">
        <div class="ibox-title">
          <h5><?= __d( 'admin', 'Menú de administración') ?></h5>
        </div>
        <div class="ibox-content">
          <div ui-tree="treeOptions" class="menu-edition-list" data-max-depth="3" id="tree-root-menu">
            <ol class="dd-list" ui-tree-nodes="" ng-model="data.content.menu">
              <li ng-repeat="el in data.content.menu" ui-tree-node ng-include="'User/fields/admin_menu_item.html'"></li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
