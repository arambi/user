<div class="tree-node tree-node-content dd-handle ">
  <span ui-tree-handle class="handle cf-tree-handler"><i class="fa fa-arrows"></i></span>
  <span class="menu-edition-title" ng-click="editSeparator(el)"><i class="{{ el.icon }}"></i> {{ el.label }}</span>
  <span ng-click="remove( $index)"><i class="fa fa-remove"></i></span>
  <div class="menu-edit row" ng-show="el.editing">
    <div class="col-sm-6">
      <label><?= __d( 'admin', 'Icono') ?></label><input type="text" ng-model="el.icon" />
    </div>
    <div class="col-sm-6">
      <label><?= __d( 'admin', 'Título') ?></label><input type="text" ng-model="el.label" />
    </div>
  </div>
</div>
<ol class="dd-list" ui-tree-nodes="" ng-model="el.nodes">
  <li ng-repeat="el in el.nodes" ui-tree-node ng-include="'User/fields/admin_menu_item.html'"></li>
</ol>