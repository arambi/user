<div class="form-group">
  <label ng-help tooltip="{{ field.help }}" class="col-lg-3 control-label">{{ field.label }}</label>
  <div class="col-lg-9">
    <div class="form-control">{{ content[field.key] }}</div>
  </div>
</div>