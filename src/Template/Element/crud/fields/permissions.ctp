<div class="form-group" ng-repeat="permission in data.permissions">
  <label class="col-sm-2 control-label"><?= __d( 'admin', 'Permisos') ?></label>
  <input type="checkbox" checklist-model="data.content.permissions" checklist-value="permission.node"> {{ permission.name }}</label>
  <div class="col-sm-10">
    <div ng-show="data.content.permissions.indexOf( permission.node) == -1 || !data.content.permissions" ng-repeat="children in permission.children" class="col-xs-offset-1">
      <label class="checkbox-inline">
        <input type="checkbox"  checklist-model="data.content.permissions" checklist-value="children.node"> {{ children.name }}</label>
      <div ng-show="data.content.permissions.indexOf( children.node) == -1 && children.children.length > 1" ng-repeat="children2 in children.children" class="col-xs-offset-1">
        <label class="checkbox-inline">
          <input type="checkbox"  checklist-model="data.content.permissions" checklist-value="children2.node"> {{ children2.name }}</label>        
      </div>
    </div>
  </div>
</div>
