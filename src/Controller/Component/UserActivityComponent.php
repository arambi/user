<?php

namespace User\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\Controller;
use Cake\Core\App;
use Cake\Core\Exception\Exception;
use Cake\Event\Event;
use Cake\Event\EventManagerTrait;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Request;
use Cake\Network\Response;
use Cake\Routing\Router;
use Cake\Utility\Hash;
use Cake\ORM\TableRegistry;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\Table;

class UserActivityComponent extends Component
{

  public function startup( Event $event)
  {
    if (strpos(strtolower(env('HTTP_USER_AGENT')), 'bot') !== false) {
      return;
    }

    $table = TableRegistry::getTableLocator()->get( 'User.UsersActivities'); 
    $log = $table->newEntity();


    $user_id = $this->_config['user'] ? $this->_config['user']['id'] : null;
    $browser = env('HTTP_USER_AGENT');
    $language = substr( env('HTTP_ACCEPT_LANGUAGE'), 0, 2);
    $domain =  env('SERVER_NAME');
    $ip = env('REMOTE_ADDR');
    $request_method = env('REQUEST_METHOD');
    $url = env('REQUEST_URI');
    $status_code = http_response_code();

    $log->set( 'user_id', $user_id);
    $log->set( 'browser', $browser);
    $log->set( 'language', $language);
    $log->set( 'domain', $domain);
    $log->set( 'ip', $ip);
    $log->set( 'request_method', $request_method);
    $log->set( 'url', substr($url, 0, 255));
    $log->set( 'status_code', $status_code);
    $log->set('data', $this->getController()->getRequest()->getData());
    $log->set('query', $this->getController()->getRequest()->getQuery());
    $table->save( $log);
  }

}