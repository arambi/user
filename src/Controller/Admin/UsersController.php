<?php

namespace User\Controller\Admin;

use Cake\Event\Event;
use Firebase\JWT\JWT;
use User\Auth\Access;
use Manager\Crud\Flash;
use Cake\Routing\Router;
use Cake\Utility\Security;
use Cake\Event\EventManager;
use User\Controller\AppController;
use Letter\Mailer\MailerAwareTrait;
use User\Controller\UsersCompleteTrait;
use Manager\Controller\CrudControllerTrait;

/**
 * Users Controller
 *
 * @property User\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{
    use CrudControllerTrait {
        initialize as crudInitialize;
    }

    use MailerAwareTrait;
    use UsersCompleteTrait;

    public $helpers = [
        'Form' => [
            'templates' => 'Cofree.bootstrap_template',
        ],
    ];

    public function initialize()
    {
        $this->crudInitialize();

        $this->Auth->allow([
            'complete',
            'forgot_password',
            'new_password',
            'token'
        ]);
    }

    public function _index($query)
    {
        $user = $this->Auth->user();
        $group_ids = $this->Users->Groups->find()
            ->where([
                'Groups.level >= ' => $user['group']['level']
            ])
            ->extract('id')
            ->toArray();

        if (!empty($group_ids)) {
            $query->where([
                'Users.group_id IN' => $group_ids
            ]);
        }

        $event = new Event('User.Controller.users.index', $this, [
            $query
        ]);

        EventManager::instance()->dispatch($event);
    }

    public function beforeFilter(Event $event)
    {
        $this->Auth->setConfig('authenticate.Form.finder', 'active');
        $this->Auth->setConfig('authenticate.Form.userModel', 'User.Users');
    }

    public function login()
    {
        $this->viewBuilder()->setLayout('Manager.blank');

        $user = $this->Users->newEntity($this->request->data);

        if ($this->Auth->user()) {
            $this->redirect($this->Auth->redirectUrl());
        }

        if ($this->request->is('post')) {

            if ($user = $this->Auth->identify()) {
                $user = $this->Users->find('auth')
                    ->where(['Users.id' => $user['id']])
                    ->contain('Groups')
                    ->first()->toArray();

                $this->Auth->setUser($user);
                $event = new Event('Acl.Controller.Users.afterLogin', $this);
                $this->getEventManager()->dispatch($event);
                $this->redirect($this->Auth->redirectUrl());
            }
        }

        $this->set(compact('user'));
    }

    public function token()
    {
        $user_auth = $this->Auth->identify();
        $user = $this->Users->find()
            ->where([
                'Users.id' => $user_auth['id']
            ])
            ->first();

        if (!$user) {
            $this->set([
                'success' => false
            ]);

            return;
        }

        $group = $this->Users->Groups->findById($user['group_id'])->first();

        $this->set([
            'success' => true,
            'token' => JWT::encode([
                'sub' => $user['id'],
                'exp' =>  time() + 604800
            ], Security::getSalt()),
            'user' => $user,
            'group' => $group->extract([
                'slug',
                'name'
            ]),
            '_serialize' => true
        ]);
    }


    public function logout()
    {
        $user = $this->Auth->user();
        $this->Auth->logout();
        // BeforeFilter Event
        $event = new Event('User.Controller.Users.afterLogoutAdmin', $this, [$user]);
        $this->getEventManager()->dispatch($event);
        $this->redirect('/admin');
    }

    public function data()
    {
        $_id = $this->Auth->user('id');
        $this->CrudTool->setAction('data');
        $query = $this->Table->find('content')->where([$this->Table->alias() . '.id' => $_id]);

        $content = $query->first();

        if ($this->request->is(['patch', 'post', 'put'])) {
            $content = $this->Table->find('content')->where([$this->Table->alias() . '.id' => $_id])->first();

            if (!$this->request->getData('change_password')) {
                unset($this->request->data['password']);
                unset($this->request->data['password2']);
            }

            $this->Table->patchContent($content, $this->request->data, ['validate' => 'editAdmin']);

            if ($this->Table->saveContent($content)) {
                Flash::success(__d('admin', 'El contenido ha sido guardado correctamente'));
            } else {
                $this->Table->crud->setEntity($content);
            }
        }

        $this->Table->crud->setContent($content);
    }

    protected function _beforeCreate(&$data)
    {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data['status'] = 'pending';
        }

        foreach (['group_id', 'email', 'username'] as $field) {
            if (empty($data[$field])) {
                $data[$field] = null;
            }
        }
    }

    protected function _afterSave($user, $created = false)
    {
        if ($created) {
            $user = $this->Users->find()
                ->contain('Groups')
                ->where([
                    'Users.id' => $user->id
                ])
                ->first();

            $hasSend = new \stdClass();
            $hasSend->send = false;

            $event = new Event('User.Controller.Users.isSendRegisterEmail', $this, [$user, $hasSend]);
            $this->getEventManager()->dispatch($event);

            if (!$hasSend->send && !empty($user->email)) {
                $this->getMailer('User.UserAdmin')->send('registerFromAdmin', [$user]);
            }
        }
    }

    public function complete($salt)
    {
        $this->viewBuilder()->setLayout('Manager.blank');
        $this->__complete($salt);
    }


    /**
     * reestablece la contraseña de un usuario enviandole por email
     * un enlace generado con su uid
     * @return void
     */
    public function forgot_password()
    {
        $this->viewBuilder()->setLayout('Manager.blank');
        $user = $this->Users->newEntity($this->request->data, ['validate' => 'forgotpassword']);

        if ($this->request->is('post')) {
            if ($user->errors()) {
                $this->Flash->error(__d('user', 'El correo electrónico ha de ser válido'), 'alert/error');
            } else {
                $user = $this->Users->find()->where(['email' => $this->request->data['email']])->first();

                if ($user == null) {
                    $this->Flash->error(__d('user', 'El correo electrónico introducido no pertenece a ningún usuario'), 'alert/error');
                } else {
                    // Generamos el enlace de recuperación
                    $salt = $this->Users->generateForgotSalt($user);

                    $url = Router::url([
                        'prefix' => 'admin',
                        'plugin' => 'User',
                        'controller' => 'Users',
                        'action' => 'new_password',
                    ]) . '/' . $user->salt . '/' . $salt;


                    // Envio del correo electrónico
                    $this->getMailer('User.UserAdmin')->send('forgot_password', [$user, Router::url($url, true)]);

                    $this->Flash->success(__d('admin', 'Te hemos enviado un correo electrónico para que puedas reestablacer la contraseña'), [
                        'plugin' => 'Manager',
                        'key' => 'success',
                    ]);

                    $this->redirect([
                        'prefix' => 'admin',
                        'plugin' => 'User',
                        'controller' => 'Users',
                        'action' => 'login'
                    ]);
                }
            }
        }
        $this->set(compact('user'));
    }

    public function new_password($salt = null, $salt_forgot = null)
    {
        $this->viewBuilder()->setLayout('Manager.blank');

        // TODO: expiración de URL
        $user = $this->Users->find()
            ->where([
                'Users.salt' => $salt,
                'Users.salt_forgot' => $salt_forgot
            ])
            ->first();

        if (!$user) {
            $this->render('expire');
        } else {
            $user = $this->Users->patchEntity($user, $this->request->data, ['validate' => 'newpassword']);

            if ($this->request->is(['patch', 'post', 'put'])) {
                if ($this->Users->save($user)) {
                    $this->Flash->success(__d('admin', 'Se ha cambiado correctamente la contraseña'), [
                        'plugin' => 'Manager',
                        'key' => 'success',
                    ]);

                    $user->set('salt_forgot', null);
                    $user->dirty('password');
                    $this->Users->save($user);

                    $this->redirect([
                        'prefix' => 'admin',
                        'plugin' => 'User',
                        'controller' => 'Users',
                        'action' => 'login'
                    ]);
                } else {
                    $this->Flash->error(__d('user', 'No se ha podido cambiar la contraseña'), 'alert/error');
                }
            }
        }

        $this->set(compact('user'));
    }


    protected function _update($content)
    {
        if ($content->status == 'pending') {
            $is_admin = (in_array('master', (array)$content->group->permissions) || in_array('master.admin', (array)$content->group->permissions));


            if ($is_admin) {
                $url = Router::url([
                    'prefix' => 'admin',
                    'plugin' => 'User',
                    'controller' => 'Users',
                    'action' => 'complete',
                    $content->salt
                ], true);

                $content->set('confirm_url', $url);
            } else {
                $url = $this->loadComponent('Section.Section')->url([
                    'prefix' => false,
                    'plugin' => 'User',
                    'controller' => 'Users',
                    'action' => 'complete',
                ]);

                $content->set('confirm_url', Router::url($url, true) . '/' . $content->salt);

                $event = new Event('User.Controller.Users.confirmUrl', $this, [$content]);
                $this->getEventManager()->dispatch($event);
            }

            $this->Table->crud->setContent([
                'confirm_url' => $content->confirm_url
            ]);
        }
    }
}
