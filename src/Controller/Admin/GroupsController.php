<?php
namespace User\Controller\Admin;

use User\Controller\AppController;
use Manager\Controller\CrudControllerTrait;
use User\Auth\Access;

/**
 * Groups Controller
 *
 * @property User\Model\Table\GroupsTable $Groups
 */
class GroupsController extends AppController 
{
	//public $components = ['Manager.CrudTool']; // TODO: eliminar

  use CrudControllerTrait;

  protected function _update()
  {
  	$this->CrudTool->addSerialized( [
      'permissions' => Access::getOptions()
    ]); 
  }

  public function export( $group_id)
  {
    $this->CrudTool->serializeAction( false);
    $this->loadModel( 'User.Users');
    $users = $this->Users->find()
      ->where([
        'Users.group_id' => $group_id
      ])
      ->toArray();
    $this->viewBuilder()->className( 'CsvView.Csv');
    $this->set( compact( 'users'));
    $_serialize = 'users';
    $_header = array( 'ID', 'Nombre', 'Email');
    $_extract = array( 'id', 'name', 'email');
    $this->response->download('my_file.csv');

    $_delimiter = ';';
    $_enclosure = '"';
    $_newline = '\r\n';
    $_bom = true;

    $this->set( compact('_serialize', '_delimiter', '_enclosure', '_newline', '_eol', '_bom', '_header', '_extract'));
  }

  public function menus()
  {
    $this->index();
    $this->CrudTool->setAction( 'menus');
  }

  public function menu( $id = false)
  {
    $this->update( $id);
    $this->CrudTool->setAction( 'menu');
  }
}
