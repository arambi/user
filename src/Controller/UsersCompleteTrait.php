<?php 

namespace User\Controller;


trait UsersCompleteTrait
{

  private function __complete( $salt)
  {
    if( $this->request->is( ['patch', 'post', 'put'])) 
    {
      $user = $this->Users->find()->where([ 
        $this->Users->alias() .'.id' => $this->request->data( 'Users.id'),
        $this->Users->alias() .'.salt' => $this->request->data( 'Users.salt'),
        $this->Users->alias() .'.status' => 'pending',
      ])->first();

      $this->Users->patchEntity( $user, $this->request->data, ['validate' => 'register']);

      if( $this->Users->save( $user))
      {
        $user->status = 'active';
        $this->Users->save( $user);

        $user = $this->Users->find()
          ->where([
            'Users.id' => $user->id
          ])
          ->contain(['Groups'])
          ->first();

        $is_admin = (in_array( 'master', (array)$user->group->permissions) || in_array( 'master.admin', (array)$user->group->permissions));
          
        if( $is_admin)
        {
          $this->getMailer( 'User.UserAdmin')->send( 'postRegisterFromAdmin', [$user]);
        }
        else
        {
          $this->Flash->success( __d( 'app', 'Tus datos se han guardado y se ha enviado un correo electrónico. Ahora ya puedes entrar con tus datos al web.'));
          
          $this->getMailer( 'User.User')->send( 'register', [
              $user,
              false
          ]);
        }

        $this->redirect( $this->Section->url([
          'prefix' => $this->request->getParam( 'prefix'),
          'plugin' => 'User',
          'controller' => 'Users',
          'action' => 'login',
        ]));
      }
      else
      {

      }
    }
    else
    {
      $user = $this->Users->find()->where([
        $this->Users->alias() .'.status' => 'pending',
        $this->Users->alias() .'.salt' => $salt,
      ])->first();

      if( !$user)
      {
        $this->Section->notFound();
      }
    }
    
    $this->set( compact( 'user'));
  }
}