<?php

namespace User\Controller;

use I18n\Lib\Lang;
use Cake\Event\Event;
use Cake\Utility\Text;
use Cake\Core\Configure;
use Cake\Routing\Router;
use Website\Lib\Website;
use Cake\Utility\Security;
use Cake\ORM\TableRegistry;
use Cake\Network\Email\Email;
use Section\Routing\RouteData;
use Cake\Mailer\MailerAwareTrait;
use Cake\Http\Exception\NotFoundException;
use Cake\ORM\Exception\MissingEntityException;
use Cake\Network\Exception\BadRequestException;

trait UsersControllerTrait
{

    public function login()
    {
        if ($this->Auth->user()) {
            return $this->__redirectLoguedUser();
        }

        $registered = $this->Users->newEntity();
        $this->set(compact('registered'));
        $logued = $this->Users->newEntity($this->request->data);

        $event = new Event('User.Controller.Users.beforeLogin', $this);
        $this->eventManager()->dispatch($event);

        if ($logued = $this->Auth->identify()) {
            $this->request->getSession()->write('AppEvents.login', $logued);
            
            if ($this->Users->isEmailActivated($logued)) {
                $this->__proccessLogin($logued);
            } else {
                $this->__setSessionConfirmation($logued);
                $this->redirect(RouteData::url([
                    'plugin' => 'User',
                    'controller' => 'Users',
                    'action' => 'confirmationLink'
                ]));
            }
        } else if ($this->request->is('post')) {
            $this->set('login_error', __d('app', '¡OOOPS! Debe de haber un error en los datos introducidos. Revísalos por favor.'));
            $this->Flash->error(__d('app', '¡OOOPS! Debe de haber un error en los datos introducidos. Revísalos por favor.'), 'alert/error');
        }

        $this->set(compact('logued'));
    }

    private function __proccessLogin($logued)
    {
        $logued = $this->Users->find('auth')
            ->where([
                'Users.id' => $logued['id']
            ])
            ->first()
            ->toArray();

        $this->Auth->setUser($logued);

        $this->Users->updateLastLogin($logued['id']);

        $event = new Event('User.Controller.Users.afterLogin', $this, [$logued]);
        $this->eventManager()->dispatch($event);

        $auth_redirect = $this->Auth->redirectUrl($this->request->session()->read('Auth.redirectUrl'));

        $this->__messageGroup($this->Auth->user('group'), 'login');

        if ($auth_redirect != '/') {
            $redirect = $auth_redirect;
        } elseif (!$this->response->getHeaderLine('Location')) {
            $redirect = $this->__redirectGroup($this->Auth->user('group'), 'login');
        }

        if (!empty($redirect)) {
            if (is_string($redirect)) {
                return $this->redirect($redirect);
            }

            $url = Router::parseRequest($redirect);
            $this->redirect(RouteData::url($url));
        } else {
            $this->redirect(Configure::read('User.defaults.url_after_login'));
        }
    }

    private function __redirectGroup($group, $type)
    {
        if (empty($group['redirect_' . $type . '_section_id'])) {
            return false;
        }

        $redirect = TableRegistry::getTableLocator()->get('Section.Sections')->getUrl($group['redirect_' . $type . '_section_id']);
        return $redirect;
    }

    private function __messageGroup($group, $type)
    {
        $key = $type . '_flash';
        $key_message = $type . '_flash_message';

        if ($group[$key] && !empty($group[$key_message])) {
            $this->Flash->success($group[$key_message]);
        }
    }

    public function logout()
    {
        $this->autoRender = false;

        $group = $this->Auth->user('group');

        $user = $this->Auth->user();
        $this->request->getSession()->write('AppEvents.logout', $user);
        $logout = $this->Auth->logout();

        // BeforeFilter Event
        $event = new Event('User.Controller.Users.afterLogout', $this, [$user]);
        $this->eventManager()->dispatch($event);

        $this->__messageGroup($group, 'logout');
        $redirect = $this->__redirectGroup($group, 'logout');

        // Redirige a la url marcada en el grupo
        if (!empty($redirect)) {
            if (is_string($redirect)) {
                return $this->redirect($redirect);
            }

            $url = Router::parseRequest($redirect);
            $this->redirect(RouteData::url($url));
        } else {
            $this->redirect($logout);
        }
    }


    public function edit()
    {
        if (empty($this->Auth->user('id'))) {
            throw new NotFoundException(__('Página no encontrada'));
        }

        $user = $this->Users->find()
            ->where(['Users.id' => $this->Auth->user('id')])
            ->first();

        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->Users->patchEntity($user, $this->request->data, ['validate' => 'edit']);

            if (empty($user->errors) && empty($this->request->data['Users']['password'])) {
                $user->unsetProperty('password')->unsetProperty('password2');
            }

            if ($this->Users->save($user)) {
                $userdata = $this->Users->find('auth')
                    ->where([
                        'Users.id' => $user->id
                    ])
                    ->first()
                    ->toArray();

                $this->Auth->setUser($userdata);
                $this->Flash->success(__d('app', 'La información se ha guardado correctamente'));
                $this->redirect($this->Section->url([
                    'plugin' => 'User',
                    'controller' => 'Users',
                    'action' => 'edit',
                ]));
            } else {
                $this->Flash->error(__d('app', '¡OOOPS! Debe de haber un error en los datos introducidos. Revísalos por favor.'), 'alert/error');
            }
        }

        $this->set('languages', Lang::combine('iso2', 'name'));
        $this->set(compact('user'));
    }

    public function register()
    {
        if ($this->Auth->user()) {
            $this->redirect('/');
        }

        $registered = $this->Users->newEntity($this->request->getData(), ['validate' => 'register']);

        // beforeRegister Event
        $event = new Event('User.Controller.Users.beforeRegister', $this, [$registered]);
        $this->eventManager()->dispatch($event);

        if ($this->request->is('post')) {
            $this->Users->prepareRegister($registered);
            $registered->set('locale', Lang::current('iso3'));

            if (Configure::read('User.useRecaptcha')) {
                $verify_recaptcha = $this->Recaptcha->verify();
            } else {
                $verify_recaptcha = true;
            }

            if ($verify_recaptcha && $this->Users->save($registered)) {
                if (!Configure::read('User.dontSendEmailWhenRegisterToUser')) {
                    $this->getMailer('User.User')->send('register', [
                        $registered,
                    ]);
                }

                $user = $this->Users->find('auth')
                    ->where([
                        'Users.id' => $registered->id
                    ])
                    ->first();

                $this->request->getSession()->write('AppEvents.register', $user->toArray());

                if (Website::get('settings.users_logued_after_login')) {
                    $this->Users->save($registered);
                    $this->Auth->setUser($user->toArray());
                }

                if (!Configure::read('User.dontSendEmailWhenRegister')) {
                    $this->getMailer('User.User')->send('newUser', [
                        $user
                    ]);
                }

                // AfterRegister Event
                $event = new Event('User.Controller.Users.afterRegister', $this, [$registered]);
                $this->eventManager()->dispatch($event);

                // Loguear o no loguear esa es la cuestión
                if (Website::get('settings.users_logued_after_login')) {
                    $this->__messageGroup($this->Auth->user('group'), 'register');

                    if ($this->request->session()->check('Auth.redirect')) {
                        return $this->redirect($this->request->session()->read('Auth.redirect'));
                    }

                    $redirect = $this->__redirectGroup($this->Auth->user('group'), 'register');

                    if (!empty($redirect)) {
                        if (is_string($redirect)) {
                            return $this->redirect($redirect);
                        }

                        $url = Router::parseRequest($redirect);
                        $this->redirect(RouteData::url($url));
                    } else {
                        $this->redirect(Configure::read('User.defaults.url_after_login'));
                    }
                } else {
                    $this->Flash->success(__('¡Gracias por registrarte! Te hemos enviado un email para verificar tu cuenta.'), 'alert/success');
                    $this->request->data = null;
                    $this->__redirectToLogin();
                }
            } else {
                $this->Flash->error(__('No ha sido posible realizar el registro correctamente. Por favor, inténtalo de nuevo.'), 'alert/error');
            }
        }

        $groups = $this->Users->Groups->find()->combine('id', 'name');
        $this->set(compact('registered', 'groups'));
    }


    public function confirm_register($salt, $salt_register)
    {
        $user = $this->Users->find()->where(['Users.salt' => $salt])->first();

        if ($user == null) {
            $this->Flash->error(__d('user', 'El usuario no existe'), 'alert/error');
            $this->redirect(array('action' => 'register'));
        } else {
            if ($user->confirmed_email || $user->salt_register == null) {
                $this->Flash->success(__d('user', 'Este enlace ya no está disponible'), 'alert/success');
                $this->__redirectToLogin();
            } else {
                if ($user->salt == $salt && $user->salt_register == $salt_register) {
                    $this->Users->setActivatedEmail($user);
                    $this->Flash->success(__d('user', 'Tu usuario ha sido activado correctamente'), 'alert/success');
                    $this->__redirectToLogin();
                } else {
                    $this->Flash->error(__d('user', 'El usuario no existe'), 'alert/error');
                }
            }
        }
    }

    private function __redirectToLogin()
    {
        $this->redirect($this->Section->url([
            'plugin' => 'User',
            'controller' => 'Users',
            'action' => 'login',
        ]));
    }

    public function confirmationLink()
    {
        $user = $this->__getSessionConfirmation();
        $user = $this->Users->resetConfirmEmail($user);
        if ($this->request->getQuery('send') && $user) {
            $this->getMailer('User.User')->send('confirmationLink', [
                $user,
            ]);

            $this->__deleteSessionConfirmation();
            $this->set('send', true);
        }

        $this->set(compact('user'));
    }

    private function __getSessionConfirmation()
    {
        $user = $this->request->getSession()->read('UserConfirmation');

        if (!$user) {
            return;
        }

        $user = $this->Users->get($user['id']);
        return $user;
    }

    private function __setSessionConfirmation($user)
    {
        $this->request->getSession()->write('UserConfirmation', $user);
    }

    private function __deleteSessionConfirmation()
    {
        $this->request->getSession()->delete('UserConfirmation');
    }


    /**
     * This action exists just to ensure AuthComponent fetches user info from
     * hybridauth after successful login
     *
     * Hyridauth's `hauth_return_to` is set to this action.
     *
     * @return \Cake\Network\Response
     */
    public function authenticated()
    {
        $user = $this->Auth->identify();
        if ($user) {
            $this->Auth->setUser($user);
            return $this->redirect($this->Auth->redirectUrl());
        }
        return $this->redirect($this->Auth->config('loginAction'));
    }

    private function __redirectLoguedUser()
    {
        if ($this->request->getQuery('redirect')) {
            return $this->redirect($this->request->getQuery('redirect'));
        }

        $redirect = $this->__redirectGroup($this->Auth->user('group'), 'login');

        if (!empty($redirect)) {
            return $this->redirect($redirect);
        }

        return $this->redirect('/');
    }
}
