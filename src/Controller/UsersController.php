<?php
namespace User\Controller;

use User\Controller\AppController;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\Core\Configure;
use Cake\Utility\Text;
use Cake\Network\Email\Email;
use Cake\Utility\Security;
use Cake\ORM\Exception\MissingEntityException;
use Cake\Network\Exception\BadRequestException;
use Section\Routing\RouteData;
use Letter\Mailer\MailerAwareTrait;
use Website\Lib\Website;
use User\Controller\UsersControllerTrait;
use User\Controller\UsersCompleteTrait;

/**
 * Users Controller
 *
 * @property User\Model\Table\UsersTable $Users
 * @property AuthComponent $Auth
 */
class UsersController extends AppController 
{
  use MailerAwareTrait;
  use UsersControllerTrait;
  use UsersCompleteTrait;
  

  public function initialize() 
  {
    parent::initialize();

    $this->Auth->allow([
      'login', 
      'logout', 
      'forgot_password', 
      'admin_forgot_password',
      'register', 
      'activate_password', 
      'admin_activate_password',
      'confirm_register', 
      'confirm_email_update',
      'confirmationLink',
      'new_password',
      'change_password',
      'sociallogin',
      'endpoint',
      'complete',
      'authenticated',
      'ses'
    ]);

    // Recaptcha
    $this->loadComponent( 'Cofree.Recaptcha');

    // BeforeFilter Event
    $event = new Event( 'Acl.Controller.Users.beforeFilter', $this);
    $this->getEventManager()->dispatch($event);

  }


  /**
   * reestablece la contraseña de un usuario enviandole por email
   * un enlace generado con su uid
   * @return void
   */
  public function forgot_password()
  {
    $user = $this->Users->newEntity( $this->request->data, ['validate' => 'forgotpassword']);

    if( $this->request->is( 'post')) 
    {
      if( $user->errors())
      {
        $this->Flash->error( __d( 'app', 'El correo electrónico ha de ser válido'), 'alert/error');
      }
      else
      {
        $conditions = [
          'email' => $this->request->data['email']
        ];
        
        if( Configure::read( 'User.scope'))
        {
          $conditions = array_merge( $conditions, Configure::read( 'User.scope'));
        }
        
        $user = $this->Users->find()->where( $conditions)->first();
        
        if( $user == null)
        {
          $this->Flash->error( __d( 'app', 'El correo electrónico introducido no pertenece a ningún usuario'), 'alert/error');
        }
        else
        {
          // Generamos el enlace de recuperación
          $salt = $this->Users->generateForgotSalt( $user);
          
          $url = $this->Section->url([
            'plugin' => 'User',
            'controller' => 'Users',
            'action' => 'new_password',
          ]) .'/'. $user->salt .'/'. $salt;

          // Envio del correo electrónico
          $this->getMailer( 'User.User')->send( 'forgot_password', [$user, Router::url( $url, true)]);

          $this->Flash->success( __d( 'app', 'Te hemos enviado un correo electrónico para que puedas reestablacer la contraseña'), 'alert/success');
          $this->redirect( $this->Section->url([
              'plugin' => 'User',
              'controller' => 'Users',
              'action' => 'login'
          ]));
        }
      }

    }
    $this->set( compact( 'user'));
  }

  /**
   * presenta el formulario de cambio de password
   * en caso de que pasen el uid del usuario y su 
   * status sea active
   * @param  string $salt 
   * @return void
   */
  public function new_password( $salt = null, $salt_forgot = null)
  { 
    // TODO: expiración de URL
    $user = $this->Users->find()
      ->where([
          'Users.salt' => $salt, 
          'Users.salt_forgot' => $salt_forgot
      ])
      ->first();

    if( !$user)
    {
      $this->render( 'expire');
    } 
    else
    {
      $user = $this->Users->patchEntity( $user, $this->request->data, ['validate' => 'newpassword']);

      if( $this->request->is(['patch', 'post', 'put']))
      {
        // $user->set( 'password', $this->request->data['Users']['password']);
        if( $this->Users->save( $user))
        { 
          $this->Flash->success( __d( 'user', 'Se ha cambiado correctamente la contraseña'), 'alert/success');
          $this->redirect( $this->Section->url([
            'plugin' => 'User',
            'controller' => 'Users',
            'action' => 'login'
          ]));
        }
        else
        {
          $this->Flash->error( __d( 'user', 'No se ha podido cambiar la contraseña'), 'alert/error');
        }
      }
    }

    $this->set( compact( 'user'));
  }

  public function complete( $salt = null)
  {
    $this->__complete( $salt);
  }

  public function ses( $salt)
  {
    if( env('REMOTE_ADDR') != '172.28.128.1')
    {
      // return;
    }

    $user = $this->Users->find( 'auth')
      ->where([
        'Users.salt' => $salt,
        // 'Users.id' => $id
      ])
      ->first();
    
    if( !$user)
    {
      $this->Section->notFound();
    }

    $user = $user->toArray();
    $this->Auth->setUser( $user);
    // AfterLogin Event
    $event = new Event( 'User.Controller.Users.afterLogin', $this, [$user]);
    $this->eventManager()->dispatch($event);

    $this->redirect( '/');
  }

}