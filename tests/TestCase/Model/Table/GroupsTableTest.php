<?php
namespace User\Test\TestCase\Model\Table;

use Acl\Model\Behavior\AclBehavior;
use Acl\Model\Entity\Aco;
use Acl\Model\Entity\Aro;
use Acl\Model\Table\AclNodesTable;
use Acl\Model\Table\AcosTable;
use Acl\Model\Table\ArosTable;
use Cake\ORM\TableRegistry;
use User\Model\Table\GroupsTable;
use Cake\TestSuite\TestCase;
use Cake\Core\App;
use Cake\Core\Configure;
use Acl\Test\Fixture\AcosFixture;
use Acl\Test\Fixture\ArosFixture;
use Acl\Test\Fixture\ArosAcosFixture;

/**
 * User\Model\Table\GroupsTable Test Case
 */
class GroupsTableTest extends TestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = [
		'plugin.user.groups',
		'plugin.user.users',
		'plugin.user.invitations',
		'plugin.acl.acos', 'plugin.acl.aros', 'plugin.acl.aros_acos',
	];

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		Configure::write('Acl.database', 'test');
		parent::setUp();
		$config = TableRegistry::exists('Groups') ? [] : [
			'className' => 'User\Model\Table\GroupsTable',
		];
		$this->Groups = TableRegistry::get('User.Groups', $config);
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Groups);

		parent::tearDown();
	}


/**
 * Creación de un grupo
 * @return void
 */
	public function testCreateGroup()
	{
		$entity = $this->Groups->newEntity([
			'name' => 'Admin'
		]);

		if( $this->Groups->save( $entity))
		{
			$query = $this->Groups->Aro->find('all', [
				'conditions' => ['model' => $this->Groups->alias(), 'foreign_key' => $entity->id]
			]);
			
			$this->assertTrue(is_object($query));
			$result = $query->first();
			$this->assertEquals( $this->Groups->alias(), $result->model);
			$this->assertEquals( $entity->id, $result->foreign_key);
		}
		else
		{
			$this->assertTrue( false);
		}
	}


}
