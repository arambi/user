<?php
namespace User\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use User\Model\Table\InvitationsTable;
use Cake\TestSuite\TestCase;

/**
 * User\Model\Table\InvitationsTable Test Case
 */
class InvitationsTableTest extends TestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = [
		'plugin.user.invitations',
		'plugin.user.users',
		'plugin.user.groups',
		'plugin.user.new_users'
	];

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$config = TableRegistry::exists('Invitations') ? [] : ['className' => 'User\Model\Table\InvitationsTable'];
		$this->Invitations = TableRegistry::get('Invitations', $config);
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Invitations);

		parent::tearDown();
	}

/**
 * Test initialize method
 *
 * @return void
 */
	public function testInitialize() {
		$this->markTestIncomplete('Not implemented yet.');
	}

/**
 * Test validationDefault method
 *
 * @return void
 */
	public function testValidationDefault() {
		$this->markTestIncomplete('Not implemented yet.');
	}

}
