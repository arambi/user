<?php
namespace User\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use User\Model\Table\UsersTable;
use Cake\TestSuite\TestCase;
use Manager\TestSuite\CrudTestCase;
use Acl\Model\Behavior\AclBehavior;
use Acl\Model\Table\AclNodesTable;
use Cake\Core\Configure;
use Cake\Utility\Text;
use Cake\Utility\Security;
use Cake\Core\Plugin;
use Cake\Network\Email\Email;

/**
 * User\Model\Table\UsersTable Test Case
 */
class UsersTableTest extends CrudTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = [
		'plugin.user.users',
		'plugin.user.groups',
	];

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		Configure::write( 'Acl.database', 'test');
		Plugin::unload( 'Website'); // Deshabilitamos plugins que dan problemas
		Plugin::unload( 'Section');
		$config = TableRegistry::exists( 'Users') ? [] : ['className' => 'User\Model\Table\UsersTable'];
		$this->Users = TableRegistry::get( 'Users', $config);

		$this->user = [
			'id' => 115,
			'group_id' => 1,
			'username' => null,
			'name' => 'Javi',
			'slug' => null,
			'password2' => '$2y$10$KyCqU8cw2SDvufYz9TwNOOqxT5kLrVRORZEu8msIat3fBbBqFDOLq',
			'email' => 'jcalzadosanchez@gmail.com',
			'language' => null,
			'timezone' => null,
			'uid' => '46d42f35-81b7-4fb1-b111-176ee7d450b3',
			'salt' => null,
			'salt_register' => '0b111fcfc0f4f2f30a115922b7a8514258e5b8af',
			'status' => 'active',
			'last_login' => null,
		];

		$this->user2 = [
			'id' => null,
			'group_id' => null,
			'username' => null,
			'name' => null,
			'slug' => null,
			'password2' => '$2y$10$KyCqU8cw2SDvufYz9TwNOOqxT5kLrVRORZEu8msIat3fBbBqFDOLq',
			'email' => 'jcalzadosanchez@gmail.com',
			'language' => null,
			'timezone' => null,
			'uid' => null,
			'salt' => null,
			'salt_register' => null,
			'status' => null,
			'last_login' => null,
		];
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		parent::tearDown();
		unset( $this->Users );
	}

	public function testAdminIndex()
	{
		$this->Users->crud->serialize( 'index');
	}

	public function testIfuserloggedin()
	{
		$result = $this->Users->ifuserloggedin( $this->user );
		$this->assertEquals( true, $result);
	}

	public function testSetDefaultGroup()
	{
		$user = $this->Users->newEntity( $this->user2); 
		$this->Users->setDefaultGroup( $user);
		$this->assertEquals( 1, $user->group_id);
	}

	public function testsetDefaultStatus()
	{
		$user = $this->Users->newEntity( $this->user2);

		$this->Users->setDefaultStatus( 'none', $user);
		
		$this->assertEquals( 'active', $user->status);

		$this->Users->setDefaultStatus( 'first', $user);

		$this->assertEquals( 'pending', $user->status);
	}

	public function testSetUid()
	{
		$token = Text::uuid();
		$user = $this->Users->newEntity( $this->user2);
		$this->Users->setUid( $token, $user); 
		$this->assertEquals( $token, $user->uid);
	}

	public function testSetSaltRegister()
	{
		$token = Text::uuid();
		$hash = Security::hash( $token, 'sha1', true);
		$user = $this->Users->newEntity( $this->user2);
		$this->Users->setSaltRegister( $hash, $user); 
		$this->assertEquals( $hash, $user->salt_register);
	}

	public function testGenerateSendLink()
	{
		$link = $this->Users->generateSendLink(); 
		$this->assertEquals( '/es/user/users/confirm_register/'.$hash.'/'.$token, $link);
	}

	public function testgetEmailTemplate()
	{
		$result = $this->Users->getEmailTemplate( 'next');
		$this->assertEquals( 'User.registration', $result);

		$result_ = $this->Users->getEmailTemplate( 'none');
		$this->assertEquals( 'User.registration_welcome', $result_);
	}

	public function testIsActivated()
	{
		$user = $this->Users->newEntity( $this->user2);
		$activate_config = 'none';

		$result = $this->Users->isActivated( $user, $activate_config);

		$this->assertEquals( true, $result);


		$activate_config = 'first';

		$result = $this->Users->isActivated( $user, $activate_config);

		$this->assertEquals( false, $result);
	}

	public function testUpdateLastLogin()
	{
		$id = 2;
		$user = $this->Users->find()->where(['id' => $id])->first();

		$user->set( 'last_login', date( 'Y-m-d H:i:s'));
    $this->Users->save( $user);
    $user = $this->Users->find()->where(['id' => $id])->first();	
    
    $this->assertEquals( true, is_object( $user->last_login));
		$this->assertEquals( 'Cake\I18n\Time', get_class( $user->last_login));
	}

	public function testUpdateStatus()
	{
		$id = 2;
		$user = $this->Users->find()->where(['id' => $id])->first();

		$user->set( 'status', 'active');
    $user->set( 'salt_register', null);
    $this->Users->save( $user);
    $user = $this->Users->find()->where(['id' => $id])->first();	
    
    $this->assertEquals( 'active', $user->status);
		$this->assertEquals( null, $user->salt_register);
	}

	public function testGenerateForgotLink()
	{
		$id = 2;
		$user = $this->Users->find()->where(['id' => $id])->first();		

		$link = $this->Users->generateForgotSalt( $user); 
		
	}

	public function testValidateEdit()
	{
		Configure::write( 'Security.salt', '640d669f26ebe0282ece4aa2f017adbc3df82a783a23dbf11b95acda673018f6');
		$data = [
			'name' => 'Carmelo',
			'email' => 'carmelo@arambis.com',
			'password' => 'abcd1234',
		];

		$user = $this->Users->newEntity( $data, [
			'validate' => 'register'
		]);

		$this->Users->save( $user);
		
		$data = [
			'name' => 'Arturo',
			'email' => 'carmelo@arambis.com',
			'current_password' => 'abcd1234',
			'password' => '1234abcd',
			'password2' => '1234abcd'
		];

		$user = $this->Users->get( $user->id);
		$this->Users->patchEntity( $user, $data, [
				'validate' => 'edit'
		]);

		_d( $user);
	}

}
