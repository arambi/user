<?php
namespace User\Test\TestCase\Controller;

use Cake\Core\Configure;
use User\Controller\UsersController;
use Cake\TestSuite\IntegrationTestCase;
use Cake\Utility\Security;
use Cake\ORM\TableRegistry;
use User\Model\Table\UsersTable;
use Cake\Network\Request;
use Cake\Network\Response;
use Cake\Core\Plugin;
// use Cake\Controller\Controller;

/**
 * User\Controller\UsersController Test Case
 */
class UsersControllerTest extends IntegrationTestCase 
{
	/**
   * Fixtures
   *
   * @var array
   */
	public $fixtures = [
		'plugin.user.users',
		'plugin.user.groups',
		'plugin.user.invitations',
	];

	public function setUp() 
	{
    // Quita el plugin section si está leído
    if( Plugin::loaded( 'Section'))
    {
      Plugin::unload( 'Section');
    }

    // Quita el plugin section si está leído
    if( Plugin::loaded( 'Website'))
    {
      Plugin::unload( 'Website');
    }
    
		parent::setUp();

		// Configuración de base de datos
		Configure::write('Acl.database', 'test');

		// Salt para los passwords
		Security::salt( 'YJfIxfs2guVoUubWDYhG93b0qyJfIxfs2guwvniR2G0FgaC9mi');

    // Table Model
		$this->Users = TableRegistry::get( 'User.Users');

	}

  /**
   * tearDown method
   *
   * @return void
   */
	public function tearDown() 
  {
		parent::tearDown();
    unset( $this->Users);
    $_SESSION = [];    
	}

	public function login( $url)
	{
    // Usuario
		$user = $this->Users->find( 'all' )->first();
		// El password que se debe usar en los fixtures debe ser siempre "a12345"
    $this->Users->updateAll(['password' => password_hash('a12345', PASSWORD_BCRYPT)], []);
		
    // Petición POST
		$this->post( $url, [
			'email' => $user->email,
			'password' => $user->password
		]);
		return $user;
	}

  /**
   * Simula un registro para usarlo en varios tests
   * @return $email email
   */
  public function register()
  {
    // TODO: disable recaptcha component
    $email = 'cakeaaaaaaa@arambis.com';
    $data = [
        'group_id' => 1,
        'name' => 'CakePHP',
        'email' => $email,
        'password' => 'a12345',
        'password2' => 'a12345',
        'status' => 'pending',
        'test' => true
    ];
   
    $this->post( '/user/users/register', $data);  // Realiza un registro mediante post con $data
    return $email; // Devuelve el email para realizar los asserts
  }

  /**
   * Test para el login
   * @return void
   */
	public function testLogin()
	{
    $user = $this->login( '/user/users/login');
    $this->assertResponseOk();
    $this->assertRedirect( Configure::read( 'User.defaults.url_after_login') );
    $this->assertSession( 1, 'Auth.User.id');
	}

  /**
   * Test para el login del admin
   * Comprueba que se guarda en la sesión 
   * @return [type] [description]
   */
	public function testAdminLogin()
	{
		$user = $this->login( '/admin/login');
    $this->assertResponseOk();
    $this->assertRedirect( '/' );
    $this->assertSession( 1, 'Auth.User.id');
	}

  /**
   * Test para el logout
   * Se comprueba que la sessión se queda vacía después del logout
   * @return void
   */
	public function testLogout()
	{
    // Hacemos login
		$user = $this->login( '/user/users/login');
		$this->assertRedirect( Configure::read( 'User.defaults.url_after_login') );
    $this->assertSession( 1, 'Auth.User.id');

		// Petición de logout
		$this->get( '/user/users/logout');
    $this->assertResponseOk();
    $this->assertSession( null, 'Auth.User.id');
	}

  /**
   * Test para el registro de usuario
   * Se comprueba también un logueo posterior
   * @return void
   */
	// public function testRegister()
	// {
 //    $email = $this->register();
 //    $this->assertResponseOk();
 //    $this->assertRedirect( '/login');

 //    $user = $this->Users->findByEmail( $email)->first();
 //    $this->assertEquals( $email, $user->email);
 //    $this->assertEquals( 'pending', $user->status);
	// }

  /**
   * Test para la confirmación del registro de usuario
   * @return void
   */
  public function testConfirm_register()
  {
    // Usamos un usuario de la fixture con pending e uid y salt
    $user = $this->Users->findById( 2 )->first();
    $this->assertEquals( 'pending', $user->status);

    $salt_register = $user->salt_register;
    $uid = $user->uid;

    $this->get( '/user/users/confirm_register/'. $salt_register . '/' . $uid); 
  
    $this->assertResponseOk();
    
    $user = $this->Users->findById( 2 )->first();
    $this->assertEquals( 'active', $user->status);
  }

  /**
   * envia mediante post el email introducido y comprueba
   * si existe algún usuario con ese email, si es así le
   * envia un email sino un flash message
   * 
   * @return void
   */
  public function testForgot_password()
  {
    $email = 'gorka@garro.com';
    
    $user = $this->Users->findByEmail( $email)->first();

    $this->assertEquals( 'gorka@garro.com', $user->email);
  }

  /**
   * provoca un error en forgot_password para
   * controlar que devuelve en caso de error
   * @return void
   */
  public function testForgot_password_failed()
  {
    $data = [
      'email' => 'pepe@garro.com'
    ];
    
    $result = $this->post( '/user/users/forgot_password', $data);

    $this->assertResponseOk();
    $this->assertEquals( 'flash', $_SESSION['Flash']['flash']['key']);
  }

  /**
   * comprueba si el token es correcto
   * @return void
   */
  public function testNew_password()
  {
    $token = '96199e41-10ab-43a0-a52e-cb98fd4b9383a'; // uid user fixtures with id 3 
    $result = $this->get( '/user/users/new_password/'.$token);
    $this->assertResponseOk();
    $this->assertEquals( array(), $_SESSION);
  }

}