<?php
namespace User\Test\TestCase\Shell;

use User\Shell\UsersShell;
use Cake\TestSuite\TestCase;

/**
 * User\Shell\UsersShell Test Case
 */
class UsersShellTest extends TestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->io = $this->getMock('Cake\Console\ConsoleIo');
		$this->Users = new UsersShell($this->io);
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Users);

		parent::tearDown();
	}


  public function testCreate()
  {
    $this->Users->add_group();
  }
}
