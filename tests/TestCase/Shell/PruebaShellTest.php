<?php
namespace User\Test\TestCase\Shell;

use User\Shell\PruebaShell;
use Cake\TestSuite\TestCase;

/**
 * User\Shell\PruebaShell Test Case
 */
class PruebaShellTest extends TestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->io = $this->getMock('Cake\Console\ConsoleIo');
		$this->Prueba = new PruebaShell($this->io);
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Prueba);

		parent::tearDown();
	}

/**
 * Test main method
 *
 * @return void
 */
	public function testMain() {
		$this->markTestIncomplete('Not implemented yet.');
	}

}
