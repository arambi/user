<?php
namespace User\Test\TestCase\Auth;

use User\Auth\Access;
use Cake\TestSuite\TestCase;

/**
 * Manager\Controller\Component\CrudToolComponent Test Case
 */
class AccessTest extends TestCase {

/**
 * setUp method
 *
 * @return void
 */
  public function setUp() {
    parent::setUp();
   
  }

/**
 * tearDown method
 *
 * @return void
 */
  public function tearDown() {
    unset($this->CrudTool);

    parent::tearDown();
  }


  public function set()
  {
    Access::add( 'posts', [
      'name' => 'Noticias',
      'options' => [
        'update' => [
          'name' => 'Editar',
          'nodes' => [
            [
              'prefix' => 'admin',
              'plugin' => 'Blog',
              'controller' => 'posts',
              'action' => 'index',
            ],
            [
              'prefix' => 'admin',
              'plugin' => 'Blog',
              'controller' => 'posts',
              'action' => 'create',
            ],
          ],
        ]
      ]
    ]);
  }

  public function testAppendNodes()
  {
    $this->set();

    Access::appendNodes( 'posts.update', [
      [
        'prefix' => 'Admin',
        'plugin' => 'Shop',
        'controller' => 'Products',
        'action' => 'index'
      ]
    ]);

  }

  public function testRead()
  {
    $this->set();
    $data = Access::getOptions();
  }

}
