<?php
namespace User\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * UsersFixture
 *
 */
class UsersFixture extends TestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = [
		'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
		'group_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
		'username' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
		'name' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
		'slug' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
		'password' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
		'password2' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
		'email' => ['type' => 'string', 'length' => 50, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
		'language' => ['type' => 'string', 'length' => 10, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
		'timezone' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
		'uid' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
		'salt' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
		'salt_register' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
		'status' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
		'last_login' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
		'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
		'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
		'_indexes' => [
			'group_id' => ['type' => 'index', 'columns' => ['group_id'], 'length' => []],
			'email' => ['type' => 'index', 'columns' => ['email'], 'length' => []],
			'username' => ['type' => 'index', 'columns' => ['username'], 'length' => []],
		],
		'_constraints' => [
			'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
		],
		'_options' => [
			'engine' => 'InnoDB', 'collation' => 'utf8_general_ci'
		],
	];

/**
 * Records
 *
 * @var array
 */
	public $records = [
		[
			'id' => 1,
			'group_id' => 1,
			'username' => 'gorka',
			'name' => 'Gorka Garro',
			'slug' => 'gorka',
			'password' => 'a12345',
			'password2' => 'a12345',
			'email' => 'gorka@garro.com',
			'language' => 'spa',
			'timezone' => null,
			'uid' => 'Lorem ipsum dolor sit amet',
			'salt' => 'Lorem ipsum dolor sit amet',
			'status' => 'active',
			'last_login' => null,
			'created' => '2014-10-31 22:26:49',
			'modified' => '2014-10-31 22:26:49'
		],
		[
			'id' => 2,
			'group_id' => 1,
			'username' => 'pepuelo',
			'name' => 'Pepe',
			'slug' => 'pepe',
			'password' => 'a12345',
			'password2' => 'a12345',
			'email' => 'pepe@pepe.com',
			'language' => 'spa',
			'timezone' => null,
			'uid' => '96199e41-10ab-43a0-a52e-cb98fd4b9383',
			'salt' => '',
			'salt_register' => '198eddbf62eae239fd28d2727663c5b6f9580025',
			'status' => 'pending',
			'last_login' => null,
			'created' => '2014-10-31 22:26:49',
			'modified' => '2014-10-31 22:26:49'
		],
		[
			'id' => 3,
			'group_id' => 1,
			'username' => 'pepuelo2',
			'name' => 'Pepeaaa',
			'slug' => 'pepeaa',
			'password' => 'a12345',
			'password2' => 'a12345',
			'email' => 'pepeaaa@pepe.com',
			'language' => 'spa',
			'timezone' => null,
			'uid' => '96199e41-10ab-43a0-a52e-cb98fd4b9383a',
			'salt' => '',
			'salt_register' => '198eddbf62eae239fd28d2727663c5b6f9580025a',
			'status' => 'active',
			'last_login' => null,
			'created' => '2014-10-31 22:26:49',
			'modified' => '2014-10-31 22:26:49'
		],
	];

}
