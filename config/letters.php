<?php 

use Cake\Core\Configure;
use Letter\Collector\LetterCollector;

$vars = [
    'name' => 'Nombre del usuario',
    'username' => 'Nombre del usuario de acceso',
    'web_name' => 'Nombre del web',
    'email' => 'Email del usuario',
];

if (Configure::read('User.confirmEmail')) {
  $vars ['confirm_url'] = 'La URL de confirmación de contraseña';
}

LetterCollector::add( 'User.register', [
  'title' => 'Usuarios - Registro',
  'subject' => [
    'spa' => 'Bienvenido a {web_name}'
  ],
  'file' => 'user_register',
  'vars' => $vars
]);

LetterCollector::add( 'User.forgot_password', [
  'title' => 'Usuarios - Cambio de contraseña',
  'subject' => [
    'spa' => 'Cambio de contraseña en {web_name}'
  ],
  'file' => 'user_forgot_password',
  'vars' => [
    'name' => 'Nombre del usuario',
    'username' => 'Nombre del usuario de acceso',
    'web_name' => 'Nombre del web',
    'email' => 'Email del usuario',
    'url' => 'La URL del enlace para recuperar contraseña',
  ]
]);



LetterCollector::add( 'User.new_user', [
  'title' => 'Nuevo usuario',
  'subject' => [
    'spa' => 'Se ha registrado un nuevo usuario'
  ],
  'file' => 'new_user',
  'vars' => [
    'name' => 'Nombre del usuario',
    'email' => 'Email del usuario',
  ]
]);

LetterCollector::add( 'User.complete_register', [
  'title' => 'Completa registro (creado desde el admin)',
  'subject' => [
    'spa' => 'Registro en el web de {web_name}'
  ],
  'file' => 'complete_register',
  'vars' => [
    'name' => 'Nombre del usuario',
    'email' => 'Email del usuario',
    'web_name' => 'Nombre del web',
    'url' => 'Url de registro',
    'domain' => 'El dominio del web',
    'domain_url' => 'La url del web',
  ]
]);


LetterCollector::add( 'User.confirmation_link', [
  'title' => 'Email de confirmación',
  'subject' => [
    'spa' => 'Confirma tu dirección de email'
  ],
  'file' => 'confirmation_link',
  'vars' => [
    'name' => 'Nombre del usuario',
    'email' => 'Email del usuario',
    'confirm_url' => 'La URL de confirmación de contraseña'
  ]
]);
