<?php

use Phinx\Migration\AbstractMigration;

class GroupsMenu extends AbstractMigration
{

  public function change()
  {
    $groups = $this->table( 'groups');
    $groups
      ->addColumn( 'menu', 'text', ['null' => true, 'default' => null])
      ->save();
  }
}
