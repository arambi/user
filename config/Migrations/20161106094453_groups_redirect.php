<?php

use Phinx\Migration\AbstractMigration;

class GroupsRedirect extends AbstractMigration
{

  public function change()
  {
    $groups = $this->table( 'groups');
    $groups
      ->addColumn( 'redirect_login_type', 'string', ['null' => true, 'default' => null, 'limit' => 16])
      ->addColumn( 'redirect_logout_type', 'string', ['null' => true, 'default' => null, 'limit' => 16])
      ->addColumn( 'redirect_login_section_id', 'integer', ['null' => true, 'default' => null])
      ->addColumn( 'redirect_logout_section_id', 'integer', ['null' => true, 'default' => null])
      ->save();
  }
}
