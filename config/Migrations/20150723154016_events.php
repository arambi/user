<?php

use Phinx\Migration\AbstractMigration;
use Cake\Event\EventManager;
use Cake\Event\Event;



class Events extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
    
    public function change()
    {

    }
     */
    public function up()
    {
      $users = $this->table( 'users');
      $event = new Event( 'Migration.User.up', $this, [
          'users' => $users
      ]);
      EventManager::instance()->dispatch( $event);
    }

    public function down()
    {
      $users = $this->table( 'users');
      $event = new Event( 'Migration.User.down', $this, [
          'users' => $users
      ]);
      EventManager::instance()->dispatch( $event);
    }
}
