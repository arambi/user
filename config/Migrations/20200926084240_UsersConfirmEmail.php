<?php

use Migrations\AbstractMigration;

class UsersConfirmEmail extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $users = $this->table('users');

        if (!$users->hasColumn('confirmed_email')) {
            $users
                ->addColumn('confirmed_email', 'boolean', ['null' => false, 'default' => 0])
                ->addIndex('confirmed_email')
                ->update();
        }

        if (!$users->hasColumn('confirm_email_request_date')) {
            $users
                ->addColumn('confirm_email_request_date', 'datetime', ['null' => true, 'default' => null])
                ->update();
        }

        if (!$users->hasColumn('confirm_email_date')) {
            $users
                ->addColumn('confirm_email_date', 'datetime', ['null' => true, 'default' => null])
                ->update();
        }
        
    }
}
