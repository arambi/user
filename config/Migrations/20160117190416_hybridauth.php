<?php

use Phinx\Migration\AbstractMigration;

class Hybridauth extends AbstractMigration
{

  public function up()
  {
    $users = $this->table( 'users');
    $users
      ->addColumn( 'provider', 'string', ['null' => true, 'default' => null, 'limit' => 100])
      ->addColumn( 'provider_uid', 'string', ['null' => true, 'default' => null, 'limit' => 255])
      ->update();
  }

  public function down()
  {
    $users = $this->table( 'users');
    $users->removeColumn( 'provider');
    $users->removeColumn( 'provider_uid');
    $users->update();
  }
}
