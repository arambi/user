<?php

use Phinx\Migration\AbstractMigration;

class Initial extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
    
    public function change()
    {
      $this->table( 'users')->addColumn( 'salt_forgot', 'string', ['null' => true, 'default' => null])->update();
    }
    */
    /*
     * Migrate Up.
     */
    public function up()
    {
      $users = $this->table( 'users');
      $users
          ->addColumn( 'group_id', 'integer', ['null' => false])
          ->addColumn( 'username', 'string', ['null' => true, 'default' => null])
          ->addColumn( 'name', 'string', ['null' => true, 'default' => null])
          ->addColumn( 'slug', 'string', ['null' => true, 'default' => null])
          ->addColumn( 'password', 'string', ['null' => true, 'default' => null])
          ->addColumn( 'password2', 'string', ['null' => true, 'default' => null])
          ->addColumn( 'email', 'string', ['null' => true, 'default' => null, 'limit' => 50])
          ->addColumn( 'timezone', 'string', ['null' => true, 'default' => null])
          ->addColumn( 'uid', 'string', ['null' => true, 'default' => null])
          ->addColumn( 'salt', 'string', ['null' => true, 'default' => null])
          ->addColumn( 'salt_register', 'string', ['null' => true, 'default' => null])
          ->addColumn( 'salt_forgot', 'string', ['null' => true, 'default' => null])
          ->addColumn( 'status', 'string', ['null' => true, 'default' => null])
          ->addColumn( 'last_login', 'datetime', ['null' => true, 'default' => null])
          ->addColumn( 'birthday', 'date', ['null' => true, 'default' => null])
          ->addColumn( 'gender', 'string', ['limit' => 2, 'null' => true, 'default' => null])
          ->addColumn( 'currency', 'string', ['limit' => 3, 'null' => true, 'default' => null])
          ->addColumn( 'locale', 'string', ['limit' => 5, 'null' => true, 'default' => null])
          ->addColumn( 'howknow', 'string', ['null' => true, 'default' => null])
          ->addColumn( 'subscribe_newsletter', 'boolean', ['null' => true, 'default' => null])
          ->addColumn( 'created', 'datetime', ['default' => null])
          ->addColumn( 'modified', 'datetime', ['default' => null])
          ->addIndex( ['group_id'])
          ->addIndex( ['email'])
          ->addIndex( ['username'])
          ->save();

      $groups = $this->table( 'groups');
      $groups
          ->addColumn( 'name', 'string', ['null' => true, 'default' => null])
          ->addColumn( 'slug', 'string', ['null' => true, 'default' => null])
          ->addColumn( 'level', 'integer', ['null' => true, 'default' => null])
          ->addColumn( 'permissions', 'text', ['null' => true, 'default' => null])
          ->addColumn( 'by_default', 'boolean', ['null' => true, 'default' => null])
          ->addColumn( 'redirect_login', 'string', ['null' => true, 'default' => null])
          ->addColumn( 'redirect_logout', 'string', ['null' => true, 'default' => null])
          ->addColumn( 'salt', 'string', ['null' => true, 'default' => null])
          ->addColumn( 'created', 'datetime', ['default' => null])
          ->addColumn( 'modified', 'datetime', ['default' => null])
          ->save();

      $invitations = $this->table( 'invitations');
      $invitations
          ->addColumn( 'user_id', 'integer', ['null' => false])
          ->addColumn( 'new_user_id', 'integer', ['null' => true, 'default' => null])
          ->addColumn( 'name', 'string', ['null' => true, 'default' => null])
          ->addColumn( 'email', 'string', ['null' => true, 'default' => null, 'limit' => 50])
          ->addColumn( 'body', 'text', ['null' => true, 'default' => null])
          ->addColumn( 'salt', 'string', ['null' => true, 'default' => null])
          ->addColumn( 'created', 'datetime', ['default' => null])
          ->addColumn( 'modified', 'datetime', ['default' => null])
          ->addIndex( ['user_id'])
          ->addIndex( ['new_user_id'])
          ->addIndex( ['email'])
          ->save();

      $users_activities = $this->table( 'users_activities');
      $users_activities
          ->addColumn( 'user_id', 'integer', ['null' => false])
          ->addColumn( 'browser', 'text', ['null' => true, 'default' => null])
          ->addColumn( 'language', 'string', ['null' => true, 'default' => null])
          ->addColumn( 'domain', 'string', ['null' => true, 'default' => null])
          ->addColumn( 'ip', 'string', ['null' => true, 'default' => null])
          ->addColumn( 'request_method', 'string', ['null' => true, 'default' => null])
          ->addColumn( 'url', 'string', ['null' => true, 'default' => null])
          ->addColumn( 'status_code', 'string', ['null' => true, 'default' => null])
          ->addColumn( 'date', 'datetime', ['default' => null])
          ->save();   
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
      $this->dropTable( 'users');
      $this->dropTable( 'groups');
      $this->dropTable( 'invitations');
      $this->dropTable( 'users_activities');
    }
}