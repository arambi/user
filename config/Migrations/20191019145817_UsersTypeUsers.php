<?php
use Migrations\AbstractMigration;

class UsersTypeUsers extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $users = $this->table( 'users');

    if( !$users->hasColumn( 'user_type'))
    {
      $users->addColumn( 'user_type', 'string', ['limit' => 20, 'null' => true, 'default' => null]);
    }
  }
}
