<?php
use Migrations\AbstractMigration;

class Messages extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $groups = $this->table( 'groups');
    $groups
      ->addColumn( 'redirect_register_type', 'string', ['null' => true, 'default' => null, 'limit' => 16])
      ->addColumn( 'redirect_register_section_id', 'integer', ['null' => true, 'default' => null])
      ->addColumn( 'login_flash', 'boolean', ['null' => true, 'default' => null])
      ->addColumn( 'login_flash_message', 'text', ['null' => true, 'default' => null])
      ->addColumn( 'register_flash', 'boolean', ['null' => true, 'default' => null])
      ->addColumn( 'register_flash_message', 'text', ['null' => true, 'default' => null])
      ->addColumn( 'logout_flash', 'boolean', ['null' => true, 'default' => null])
      ->addColumn( 'logout_flash_message', 'text', ['null' => true, 'default' => null])
      ->save();
    
    $groups_translate = $this->table( 'groups_translations', ['id' => false, 'primary_key' => ['id', 'locale']]);
    $groups_translate
      ->addColumn( 'id', 'integer', ['null' => false])
      ->addColumn( 'locale', 'string', ['null' => true, 'default' => null, 'limit' => 5])
      ->addColumn( 'login_flash_message', 'string', ['null' => true, 'default' => null])
      ->addColumn( 'register_flash_message', 'string', ['null' => true, 'default' => null])
      ->addColumn( 'logout_flash_message', 'string', ['null' => true, 'default' => null])
      ->create();  
  }
}
