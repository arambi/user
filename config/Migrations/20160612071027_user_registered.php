<?php

use Phinx\Migration\AbstractMigration;

class UserRegistered extends AbstractMigration
{
  /**
   * Change Method.
   *
   * Write your reversible migrations using this method.
   *
   * More information on writing migrations is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
   */
  public function change()
  {
    $users = $this->table( 'users');
    $users
      // Indica que el usuario está registrado
      // Posibilita a la aplicación que haya usuarios no registrados que por ejemplo para una tienda puedan comprar sin registro
      ->addColumn( 'phone', 'string', ['null' => true, 'default' => null, 'limit' => 50])
      ->addColumn( 'registered', 'boolean', ['null' => true, 'default' => 1])
      ->addIndex( ['registered'])
      ->save();
  }
}
