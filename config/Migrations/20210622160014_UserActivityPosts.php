<?php

use Migrations\AbstractMigration;

class UserActivityPosts extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('users_activities')
            ->addColumn('data', 'text', ['null' => true, 'default' => null])
            ->addColumn('query', 'text', ['null' => true, 'default' => null])
            ->changeColumn('user_id', 'integer', ['null' => true, 'default' => null])
            ->update();
    }
}
