<?php
use Migrations\AbstractMigration;

class UsersSeveral extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $users = $this->table( 'users');

    if( !$users->hasColumn( 'company'))
    {
      $users->addColumn( 'company', 'string', ['limit' => 100, 'null' => true, 'default' => null]);
    }

    if( !$users->hasColumn( 'address'))
    {
      $users->addColumn( 'address', 'string', ['null' => true, 'default' => null]);
    }

    if( !$users->hasColumn( 'postcode'))
    {
      $users->addColumn( 'postcode', 'string', ['limit' => 16, 'null' => true, 'default' => null]);
    }

    if( !$users->hasColumn( 'city'))
    {
      $users->addColumn( 'city', 'string', ['limit' => 64, 'null' => true, 'default' => null]);
    }

    if( !$users->hasColumn( 'web'))
    {
      $users->addColumn( 'web', 'string', ['limit' => 128, 'null' => true, 'default' => null]);
    }

    if( !$users->hasColumn( 'fax'))
    {
      $users->addColumn( 'fax', 'string', ['limit' => 32, 'null' => true, 'default' => null]);
    }

    if( !$users->hasColumn( 'fax'))
    {
      $users->addColumn( 'state_id', 'integer', ['limit' => 2, 'null' => true, 'default' => null]);
    }

    if( !$users->hasColumn( 'fax'))
    {
      $users->addColumn( 'country_id', 'integer', ['limit' => 2, 'null' => true, 'default' => null]);
    }

    if( !$users->hasColumn( 'fax'))
    {
      $users->addColumn( 'vat_number', 'string', ['limit' => 16, 'null' => true, 'default' => null]);
    }

    if( !$users->hasColumn( 'finish_at'))
    {
      $users->addColumn( 'finish_at', 'datetime', ['null' => true, 'default' => null]);
    }

    if( !$users->hasColumn( 'legacy_id'))
    {
      $users->addColumn( 'legacy_id', 'integer', ['null' => true, 'default' => null])
      ->addIndex( 'legacy_id');
      
    }

    $users->update();
  }
}
