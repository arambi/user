
Hola {name}, 

Has solicitado cambiar la contraseña del acceso de {web_name}.

Ten en cuenta que esta acción cambiará tu contraseña actual.

Si realmente deseas cambiarla, por favor haz clic en este enlace: 

<a href="{url}">{url}</a> 

