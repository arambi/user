Hola {name},

Gracias por crear una cuenta de cliente en {web_name}. 

Estos son sus datos de acceso:

DIRECCIÓN EMAIL: {email}

CONTRASEÑA: No se muestra por seguridad    

Consejos de Seguridad:

* Mantenga los datos de su cuenta en un lugar seguro.

* No comparta los detalles de su cuenta con otras personas.

* Cambie su clave regularmente.

* Si sospecha que alguien está usando ilegalmente su cuenta,
avísenos inmediatamente.

