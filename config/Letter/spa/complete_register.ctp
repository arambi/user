<table align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" width="100%"><!-- Content -->
	<tbody>
		<tr>
			<td align="left" mc:edit="text_98" style="font-family: 'Muli-ExtraLight', sans-serif; letter-spacing: 0; font-weight:lighter; color:#404040; font-size:28px; line-height:27px;">Bienvenido/a a <!--?= \Website\Lib\Website::get( 'title') ?--></td>
		</tr>
		<!-- End Content -->
		<tr>
			<td height="12" style="font-size:2px; line-height:2px;">&nbsp;</td>
		</tr>
		<!-- Content -->
		<tr>
			<td align="left" style="font-family: 'Muli-Light', sans-serif; color:#404040;  line-height:1.5;">Has sido invitado a administrar el web de <span style="font-family: 'Muli-Bold', sans-serif; ">{web_name}.</span> Necesitamos que hagas el registro mediante el siguiente enlace:<br />
			<br />
			<span style="font-family: 'Muli-Bold', sans-serif; ">TU SITIO WEB:<br />
			{domain_url}<br />
			<br />
			TU DIRECCIÓN DE CORREO ELECTRÓNICO PARA INICIAR SESIÓN:<br />
			{email}</span></td>
		</tr>
		<!-- End Content -->
		<tr>
			<td height="12" style="font-size:2px; line-height:2px;">&nbsp;</td>
		</tr>
		<!-- Content -->
		<tr>
			<td align="center" class="elius_button1" mc:edit="text_99" style="font-family: 'Muli-Regular', sans-serif; font-weight:400; color:#fff;  line-height:25px;"><a class="elius_button1_tbl" href="{url}" style="display: inline-block; background: #404040; padding: 20px 60px; color: #fff; letter-spacing: 1.75px;" target="_blank">REGISTRARME</a></td>
		</tr>
		<!-- End Content -->
		<tr>
			<td height="12" style="font-size:2px; line-height:2px;">&nbsp;</td>
		</tr>
		<!-- Content -->
		<tr>
			<td align="left" mc:edit="text_99" style="font-family: 'Muli-Light', sans-serif; color:#404040;  line-height:1.5;">Una vez ahí debes rellenar el formulario. Inmediatamente después recibirás un correo electrónico con las instrucciones para acceder al administrador.</td>
		</tr>
		<!-- End Content -->
		<tr>
			<td height="10" style="font-size:2px; line-height:2px;">&nbsp;</td>
		</tr>
	</tbody>
</table>
