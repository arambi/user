<?php
use Cake\Routing\Router;

Router::plugin( 'User', function ($routes) {
    $routes->fallbacks( 'InflectedRoute');
});

/*
Router::plugin( 'User',  function( $routes) {
  $routes->routeClass( 'I18n.I18nRoute');
  $routes->fallbacks( 'InflectedRoute');
});
*/
// Router::connect( '/login', [
//   'plugin' => 'User',
//   'controller' => 'Users',
//   'action' => 'login'
// ], ['routeClass' => 'InflectedRoute']);

// Router::connect( '/register', [
//   'plugin' => 'User',
//   'controller' => 'Users',
//   'action' => 'register'
// ], ['routeClass' => 'InflectedRoute']);

// Router::connect( '/admin/login', [
//   'prefix' => 'admin',
//   'plugin' => 'User',
//   'controller' => 'users',
//   'action' => 'login'
// ], ['routeClass' => 'InflectedRoute']);
