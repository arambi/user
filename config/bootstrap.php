<?php
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Manager\Navigation\NavigationCollection;
use User\Auth\Access;
use Section\Action\ActionCollection;
use Cake\Event\EventManager;
use User\Event\UserListener;

if( file_exists( ROOT .DS. 'config' .DS. 'user.php'))
{
  Configure::load( 'user');
}

require 'letters.php';

// Event Listeners
EventManager::instance()->attach( new UserListener());


// URL ACTIONS
ActionCollection::set( 'user_login', [
  'label' => __d( 'admin', 'Login de usuarios'),
  'plugin' => 'User',
  'controller' => 'Users',
  'action' => 'login',
  'icon' => 'fa fa-user',
  // 'autocreate' => true,
  'autocreateCallback' => function(){
    return Configure::read( 'User.hasUsers');
  },
  'defaults' => [
    'title' => 'Login de usuarios',
    'menu' => 'none',
    'show_menu' => false
  ]
]);

ActionCollection::set( 'user_edit', [
  'label' => __d( 'admin', 'Datos de usuario'),
  'plugin' => 'User',
  'controller' => 'Users',
  'action' => 'edit',
  'icon' => 'fa fa-user',
  // 'autocreate' => true,
  'autocreateCallback' => function(){
    return Configure::read( 'User.hasUsers');
  },  
  'defaults' => [
    'title' => 'Datos de usuario',
    'menu' => 'none',
  ]
]);

ActionCollection::set( 'user_register', [
  'label' => __d( 'admin', 'Registro de usuarios'),
  'plugin' => 'User',
  'controller' => 'Users',
  'action' => 'register',
  'icon' => 'fa fa-user',
  // 'autocreate' => true,  
  'autocreateCallback' => function(){
    return Configure::read( 'User.hasUsers');
  },
  'defaults' => [
    'title' => 'Registro de usuarios',
    'menu' => 'none',
  ]
]);

ActionCollection::set( 'user_complete', [
  'label' => __d( 'admin', 'Completar registro'),
  'plugin' => 'User',
  'controller' => 'Users',
  'action' => 'complete',
  'wildcard' => true,
  'icon' => 'fa fa-user',
  // 'autocreate' => true,
  'autocreateCallback' => function(){
    return Configure::read( 'User.hasUsers');
  },
  'defaults' => [
    'title' => 'Completar registro',
    'menu' => 'none',
  ]
]);


ActionCollection::set( 'user_logout', [
  'label' => __d( 'admin', 'Logout de usuarios'),
  'plugin' => 'User',
  'controller' => 'Users',
  'action' => 'logout',
  'icon' => 'fa fa-user',
  // 'autocreate' => true,
  'autocreateCallback' => function(){
    return Configure::read( 'User.hasUsers');
  },
  'defaults' => [
    'title' => 'Logout de usuarios',
    'menu' => 'none',
  ]
]);

ActionCollection::set( 'user_forgot_password', [
  'label' => __d( 'admin', 'Contraseña olvidada'),
  'plugin' => 'User',
  'controller' => 'Users',
  'action' => 'forgot_password',
  'icon' => 'fa fa-user',
  // 'autocreate' => true,
  'autocreateCallback' => function(){
    return Configure::read( 'User.hasUsers');
  },
  'defaults' => [
    'title' => 'Contraseña olvidada',
    'menu' => 'none',
  ]
]);

if (Configure::read('User.confirmEmail')) {
  ActionCollection::set( 'user_confirm_email', [
    'label' => __d( 'admin', 'Confirmar email de usuario'),
    'plugin' => 'User',
    'controller' => 'Users',
    'action' => 'confirm_register',
    'icon' => 'fa fa-user',
    'wildcard' => true,
    // 'autocreate' => true,
    'defaults' => [
      'title' => 'Confirmar email de usuario',
      'menu' => 'none',
    ]
  ]);
  
  ActionCollection::set( 'user_confirmation_link', [
    'label' => __d( 'admin', 'Enviar enlace de confirmación de email'),
    'plugin' => 'User',
    'controller' => 'Users',
    'action' => 'confirmationLink',
    'icon' => 'fa fa-user',
    'wildcard' => true,
    // 'autocreate' => true,
    'defaults' => [
      'title' => 'Enviar enlace de confirmación de email',
      'menu' => 'none',
    ]
  ]);
}

ActionCollection::set( 'user_new_password', [
  'label' => __d( 'admin', 'Nueva contraseña'),
  'plugin' => 'User',
  'controller' => 'Users',
  'action' => 'new_password',
  'icon' => 'fa fa-user',
  // 'autocreate' => true,
  'autocreateCallback' => function(){
    return Configure::read( 'User.hasUsers');
  },
  'wildcard' => true,
  'defaults' => [
    'title' => 'Nueva contraseña',
    'menu' => 'none',
  ]
]);

// CONFIGURACIÓN DE LOS MENUS DE ADMINISTRACIÓN
NavigationCollection::add( [
  'name' => 'Usuarios',
  'icon' => 'fa fa-users',
  'url' => false,
  'key' => 'user'
]);



NavigationCollection::add( [
  'parent' => 'user',
  'name' => 'Usuarios',
  'parentName' => 'Usuarios',
  'plugin' => 'User',
  'controller' => 'Users',
  'action' => 'index',
  'icon' => 'fa fa-users',
]);

NavigationCollection::add( [
  'parent' => 'user',
  'name' => 'Grupos de usuario',
  'parentName' => 'Usuarios',
  'plugin' => 'User',
  'controller' => 'Groups',
  'action' => 'index',
  'icon' => 'fa fa-users',
]);

NavigationCollection::add( [
  'parent' => 'user',
  'name' => 'Menus',
  'parentName' => 'Usuarios',
  'plugin' => 'User',
  'controller' => 'Groups',
  'action' => 'menus',
  'icon' => 'fa fa-users',
]);


Access::add( 'admin', [
  'name' => 'Entrada al administrador',
    'options' => [
      'view' => [
          'name' => 'Acceso',
          'nodes' => [
            [
              'prefix' => 'admin',
              'plugin' => 'Manager',
              'controller' => 'Pages',
              'action' => 'home',
            ]
          ]
      ]
    ]    
]);

Access::add( 'groups', [
  'name' => 'Grupos de usuario',
  'options' => [
    'update' => [
      'name' => 'Editar',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'User',
          'controller' => 'groups',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'User',
          'controller' => 'groups',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'User',
          'controller' => 'groups',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'User',
          'controller' => 'groups',
          'action' => 'delete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'User',
          'controller' => 'groups',
          'action' => 'export',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'User',
          'controller' => 'groups',
          'action' => 'menus',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'User',
          'controller' => 'groups',
          'action' => 'menu',
        ],
      ]
    ]
  ]
]);

Access::add( 'users', [
  'name' => 'Usuarios',
  'options' => [
    'update' => [
      'name' => 'Editar',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'User',
          'controller' => 'users',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'User',
          'controller' => 'users',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'User',
          'controller' => 'users',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'User',
          'controller' => 'users',
          'action' => 'field',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'User',
          'controller' => 'users',
          'action' => 'delete',
        ]
      ]
    ],

  ]
]);


Access::add( 'usersfront', [
  'name' => 'Datos de usuarios del web',
  'options' => [
    'update' => [
      'name' => 'Editar',
      'nodes' => [
        [
          'plugin' => 'User',
          'controller' => 'users',
          'action' => 'edit',
        ],
      
      ]
    ],

  ]
]);