<?php

$config ['Access'] = [
  'groups' => [
    'name' => 'Grupos de usuario',
    'options' => [
      'update' => [
        'name' => 'Editar',
        'nodes' => [
          [
            'prefix' => 'admin',
            'plugin' => 'User',
            'controller' => 'groups',
            'action' => 'index',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'User',
            'controller' => 'groups',
            'action' => 'update',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'User',
            'controller' => 'users',
            'action' => 'index',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'User',
            'controller' => 'users',
            'action' => 'update',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'User',
            'controller' => 'users',
            'action' => 'create',
          ]
        ]
      ]
    ]
  ]
];