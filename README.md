User Plugin
===========

##Gestión de Usuarios en el frontend

###Dependencias

User utilitza Recaptcha plugin para los formularios que escriben datos:
```
https://github.com/cake17/cakephp-recaptcha
```

Ficheros de configuración a incluir en la app

user.php
```
use Cake\Core\Configure;

$config ['User'] = [
  'defaults' => [
    'group' => 'admin', // grupo por defecto
    'projectname' => 'development enviroment', // nombre del proyecto que aparecerá en el subject de los emails
    'url_after_login' => '/admin', // url donde redirige después de hacer login
    'email_transport' => 'gmaildev', // configuración del email que envia las notificaciones - definido en app.php
    'from' => 'cofreedevs@yahoo.com', // configuración del email que aparece como "desde" en las notificaciones - debe ser el definido como sender en email_transport
    'activate' => 'days', // first, none, days, or next, si es first necesita activación previa, si es next necesita activación pero le logueamos la primera vez si es none no necesita activación, si es days permitimos el login sin activación por X días(definido en login_without_activation_days) desde la fecha de creación del usuario
    'login_without_activation_days' => 5
  ],
  'recaptcha' => [
    'active' => true,
    'actions' => [
      'register',
    ]
  ]
];
```

recaptcha.php
```
$config = [
    'Recaptcha' => [
        // Register API keys at https://www.google.com/recaptcha/admin
        'siteKey' => '6Lfx2QETAAAAAG1Z0V8GB0ZDJ9Rw3SzqE0dO571X',
        'secret' => '6Lfx2QETAAAAADlSGRswyYIuQmowOeJAv2cqvAES',
        // reCAPTCHA supported 40+ languages listed here: https://developers.google.com/recaptcha/docs/language
        'defaultLang' => 'es',
        // either light or dark
        'defaultTheme' => 'light',
        // either image or audio
        'defaultType' => 'image',
    ]
];
```

Carga del plugin y sus dependencias en el bootstrap de la aplicación

bootstrap.php
```
Plugin::load( 'User', ['bootstrap' => true, 'routes' => true, 'autoload' => true]);
Plugin::load( 'Recaptcha', ['routes' => false, 'bootstrap' => true, 'autoload' => true]);
```

Configuración del correo
En App.php configurar el EmailTransport
En config/user especificar el Emailtransport: 'email_transport' => 'gmail' especificando el nombre del transporte


###Convenciones entorno a los usuarios

Registro de Usuarios
- Los usuarios han de introducir un email válido 
- Los usuarios han de introducir una contraseña con 5 o más caracteres y ha de contener al menos 1 letra y 1 número
- El Email debe ser único

Activación de usuarios
- Los usuarios han de cambiar mediante un enlace su status a active
- En la configuración user.php se especifica que dinámica de activación/login tienen los usuario:
  first: necesita activación previa
  next: necesita activación pero le logueamos la primera vez
  none: no necesita activación y lo logueamos
  days: permitimos el login sin activación por X días (definido en login_without_activation_days) desde la fecha de creación del usuario

Recuperación de contraseña

Component LogActivity
pep
