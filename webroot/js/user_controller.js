;(function(){
  function AdminMenuCtrl( $scope, $http, $timeout, $modal, cfSpinner) {
    if( !$scope.data.content.menu){
      $scope.data.content.menu = [];
    }

    $scope.separators = [
      {
        key: "separator",
        label: "Separador",
        type: 'separator',
        nodes: []
      }
    ];

    $scope.treeOptions = {
      dragStop: function( event) {

      }
    };

    $scope.editSeparator = function( item){
      item.editing = !item.editing;
    }

    $scope.remove = function( $index){
      $scope.data.content.menu.splice( $index, 1);
    }
  }


  angular
        .module('admin')
        .controller( 'AdminMenuCtrl', AdminMenuCtrl)
    ;
})()